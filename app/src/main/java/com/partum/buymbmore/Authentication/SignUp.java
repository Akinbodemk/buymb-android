package com.partum.buymbmore.Authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseAuthInvalidCredentialsException;
import com.google.firebase.auth.FirebaseAuthUserCollisionException;
import com.google.firebase.auth.FirebaseAuthWeakPasswordException;
import com.google.firebase.firestore.FirebaseFirestore;
import com.partum.buymbmore.HomeActivity;
import com.partum.buymbmore.R;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

public class SignUp extends AppCompatActivity {

    private EditText fullNameEditText,emailEditText,passwordEditText,pinEditText;
    private String  email,fullname,  password, pin;
    private Button signup;
    private AVLoadingIndicatorView loadingIndicatorView;
    private FirebaseAuth mAuth;
    private TextView gotanAccount;
    private FirebaseFirestore db;
    private LinearLayout signUpLayout,codeLayout;
    private Button submitCode;
    private TextView resendCode;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        emailEditText = findViewById(R.id.user_email);
        fullNameEditText = findViewById(R.id.full_name);
        passwordEditText = findViewById(R.id.user_password);
        pinEditText = findViewById(R.id.wallet_pin);
        signup = findViewById(R.id.signup);
        gotanAccount = findViewById(R.id.got_an_account);
        signUpLayout = findViewById(R.id.signup_layout);
        codeLayout = findViewById(R.id.code_layout);
        submitCode = findViewById(R.id.submit_code);
        resendCode = findViewById(R.id.resend_code);
        loadingIndicatorView = findViewById(R.id.signup_progress);
        mAuth = FirebaseAuth.getInstance();
        db = FirebaseFirestore.getInstance();




        gotanAccount.setOnClickListener(v -> {
            startActivity( new Intent(SignUp.this,Login.class));
            finish();
        });



        signup.setOnClickListener( v -> {

            if(TextUtils.isEmpty(fullNameEditText.getText()) || TextUtils.isEmpty(emailEditText.getText()) || TextUtils.isEmpty(passwordEditText.getText()) || TextUtils.isEmpty(pinEditText.getText())){
                Toast.makeText(SignUp.this,"Fields must be complete",Toast.LENGTH_SHORT).show();
            }
            else {
                fullname = fullNameEditText.getText().toString();
                email = emailEditText.getText().toString();
                password = passwordEditText.getText().toString();
                pin = pinEditText.getText().toString();
                signup.setVisibility(View.INVISIBLE);
                loadingIndicatorView.setVisibility(View.VISIBLE);
                createAccount(email,password);
            }
        });
    }

    public void createAccount(String user_email, String user_password){
        mAuth.createUserWithEmailAndPassword(user_email, user_password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {

                            Log.d("SIGNUP", "createUserWithEmail:success");

                            saveDetails(email,fullname,pin);
                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("SIGNUP", "createUserWithEmail:failure", task.getException());
                            Toast.makeText(SignUp.this, task.getException().getMessage(),
                                    Toast.LENGTH_SHORT).show();
                            signup.setVisibility(View.VISIBLE);
                            loadingIndicatorView.setVisibility(View.GONE);

                        }

                        // ...
                    }
                });
    }

    public void saveDetails(String user_email, String fullname, String pin){
        Map<String, Object> user = new HashMap<>();
        user.put(getString(R.string.emailField), user_email);
        user.put(getString(R.string.fullnameField),fullname);
        user.put(getString(R.string.walletPinField),pin);
        user.put(getString(R.string.walletBalanceField),0);

        db.collection(getString(R.string.users)).document().set(user)
                .addOnSuccessListener(new OnSuccessListener<Void>() {
                    @Override
                    public void onSuccess(Void aVoid) {
                        Log.d("SIGNUP", "DocumentSnapshot successfully written!");
                        startActivity(new Intent(SignUp.this, HomeActivity.class));
                        finish();
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("SIGNUP", "Error writing document", e);
                        Toast.makeText(SignUp.this,"Succesful",Toast.LENGTH_SHORT).show();
                    }
                });
    }

    public void sendCode(String email){

    }
}
