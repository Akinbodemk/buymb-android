package com.partum.buymbmore.Authentication;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.partum.buymbmore.HomeActivity;
import com.partum.buymbmore.R;
import com.wang.avi.AVLoadingIndicatorView;

public class Login extends AppCompatActivity {

    private Button login;
    private AVLoadingIndicatorView loader;
    private EditText emailEditText, passwordEditText;
    private FirebaseAuth mAuth;
    private TextView forgotPassword;
    private String TAG = "LOGIN";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        login = findViewById(R.id.login);
        emailEditText = findViewById(R.id.email);
        passwordEditText = findViewById(R.id.password);
        loader = findViewById(R.id.login_progress);
        forgotPassword = findViewById(R.id.forgot_password);
        mAuth = FirebaseAuth.getInstance();




        forgotPassword.setOnClickListener(v -> {

            if(TextUtils.isEmpty(emailEditText.getText())){
                Toast.makeText(Login.this,"Please enter your email address",Toast.LENGTH_SHORT).show();
            }else{
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Forgot Password")
                        .setMessage("A password reset link would be sent to the email address you just entered")
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                sendResetEmail(emailEditText.getText().toString());
                                dialog.dismiss();
                            }
                        })
                        .create();
                dialog.show();
            }

        });



        login.setOnClickListener(v -> {
           if(TextUtils.isEmpty(emailEditText.getText()) || TextUtils.isEmpty(emailEditText.getText())) {
               Toast.makeText(Login.this,"Fields can not be empty",Toast.LENGTH_SHORT).show();
           }
           else{
               login.setVisibility(View.INVISIBLE);
               loader.setVisibility(View.VISIBLE);
               loginUser(emailEditText.getText().toString(), passwordEditText.getText().toString());
           }
        });
    }

    private void loginUser(String email, String password){
        mAuth.signInWithEmailAndPassword(email, password)
                .addOnCompleteListener(this, new OnCompleteListener<AuthResult>() {
                    @Override
                    public void onComplete(@NonNull Task<AuthResult> task) {
                        if (task.isSuccessful()) {
                            // Sign in success, update UI with the signed-in user's information
                            Log.d("LOGIN", "signInWithEmail:success");
                         startActivity(new Intent(Login.this, HomeActivity.class));
                         finish();

                        } else {
                            // If sign in fails, display a message to the user.
                            Log.w("LOGIN", "signInWithEmail:failure", task.getException());
                            Toast.makeText(Login.this, "Authentication failed.",
                                    Toast.LENGTH_SHORT).show();
                            loader.setVisibility(View.GONE);
                            login.setVisibility(View.VISIBLE);

                        }

                        // ...
                    }
                });

    }

    private void sendResetEmail(String email){

        forgotPassword.setText("Reset Link Sent");
        mAuth.sendPasswordResetEmail(email)
                .addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task) {
                        if (task.isSuccessful()) {
                            Log.d(TAG, "Email sent.");
                        }
                    }
                });
    }
}
