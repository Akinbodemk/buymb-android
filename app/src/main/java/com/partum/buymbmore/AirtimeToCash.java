package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class AirtimeToCash extends AppCompatActivity {

    private ImageView back;
    private RelativeLayout glo,airtel,mtn,etisalat;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_airtime_to_cash);

        back = findViewById(R.id.back_to_home_from_airtime_to_cash);
        back.setOnClickListener(v -> {
            onBackPressed();
        });

        glo = findViewById(R.id.glo);
        airtel = findViewById(R.id.airtel);
        mtn = findViewById(R.id.mtn);
        etisalat = findViewById(R.id.etisalat);

        glo.setOnClickListener(v -> {
            Intent intent = new Intent(AirtimeToCash.this,PerformSwap.class);
            intent.putExtra("NETWORK_NAME","GLO");
            startActivity(intent);
        });

        mtn.setOnClickListener(v -> {
            Intent intent = new Intent(AirtimeToCash.this,PerformSwap.class);
            intent.putExtra("NETWORK_NAME","MTN");
            startActivity(intent);
        });

        airtel.setOnClickListener(v -> {
            Intent intent = new Intent(AirtimeToCash.this,PerformSwap.class);
            intent.putExtra("NETWORK_NAME","AIRTEL");
            startActivity(intent);
        });

        etisalat.setOnClickListener(v -> {
            Intent intent = new Intent(AirtimeToCash.this,PerformSwap.class);
            intent.putExtra("NETWORK_NAME","ETISALAT");
            startActivity(intent);
        });


    }
}
