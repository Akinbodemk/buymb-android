package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.ImageView;

public class BuyPhone extends AppCompatActivity {

    private ImageView back, buyIphome, buySamsung;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_phone);

        back = findViewById(R.id.back_from_buy_phone);
        buyIphome = findViewById(R.id.buy_iphone);
        buySamsung = findViewById(R.id.buy_samsung);


        back.setOnClickListener( v -> {

            onBackPressed();
        });


        buyIphome.setOnClickListener(v ->{
            Intent intent = new Intent(BuyPhone.this,PhoneList.class);
            intent.putExtra("PHONE_TYPE",0);
            startActivity(intent);
                }
        );


        buySamsung.setOnClickListener( v -> {
            Intent intent = new Intent(BuyPhone.this,PhoneList.class);
            intent.putExtra("PHONE_TYPE",1);
            startActivity(intent);
        });
    }
}
