package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

public class Utility extends AppCompatActivity {

    private ImageView back;
    private CardView cabletv;
    private CardView electricity;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_utility);
        back = findViewById(R.id.back_from_utilities);
        cabletv = findViewById(R.id.cabletv);
        electricity = findViewById(R.id.electricity);

        back.setOnClickListener(v -> {
            onBackPressed();
        });
        cabletv.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivity(new Intent(Utility.this, BuyTV.class));
            }
        });
        electricity.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {startActivity(new Intent(Utility.this, BuyElectricity.class));
            }
        });
    }
}
