package com.partum.buymbmore.Models;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.google.gson.JsonObject;
import com.partum.buymbmore.EventModels.DataEventModel;
import com.partum.buymbmore.Network.DataClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Data {
    private String phoneNumber;
    private int networkID;
    private int planID,amountToPay;
    private String reference;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private String email;


    public Data(String phoneNumber, int networkID, int planID, String reference, int amountToPay) {
        this.phoneNumber = phoneNumber;
        this.networkID = networkID;
        this.planID = planID;
        this.reference = reference;
        this.amountToPay = amountToPay;
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();
    }

    public void BuyData(){
        Log.d("BUY_DATA_FIELDS","networkID: "+ networkID + " planID: "+ planID+ " phoneNumber: "+ phoneNumber);
        JsonObject jsonObject = new JsonObject();
        jsonObject.addProperty("network_id", networkID);
        jsonObject.addProperty("plan_id", planID);
        jsonObject.addProperty("phone", phoneNumber);

        Call<ResponseBody> call = DataClient.getInstance().getDataAPI().purchaseData("Bearer c87d37705e3c4ba6eea803212d42351706a670f2c67d2e0fa82f835e07b1f741",jsonObject);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {

                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.d("DATA RESPONSE",jsonObject.toString());
                        boolean status = jsonObject.getBoolean("status");
                        if(status){
                            JSONObject data = jsonObject.getJSONObject("data");
                            String newReference = data.getString("reference");
                            updateReference(newReference);
                            Wallet wallet = new Wallet("Debit",String.valueOf(amountToPay));
                            wallet.applyChanges();
                        }
                        else{
                             updateStatus();
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("VERIFIER",errorObject.toString());
                        //call event bus
                        updateStatus();
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                //call event bus
                updateStatus();
                Log.d("VERIFIER",t.getMessage());
            }
        });
    }

    private void updateReference(String newreference){
        Map<String, Object> updateData = new HashMap<>();
        updateData.put("reference",newreference);
        updateData.put("status","Successful");
        db.collection("Transactions")
                .whereEqualTo("reference",reference)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference dRef = document.getReference();
                                dRef
                                        .update(updateData)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                EventBus.getDefault().post(new DataEventModel("Transaction Failed",true));
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                //calleventbus
                                                updateStatus();
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });
                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void updateStatus(){
        EventBus.getDefault().post(new DataEventModel("Transaction Failed",false));
        db.collection("Transactions")
                .whereEqualTo("reference",reference)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference dRef = document.getReference();
                                dRef
                                        .update("status", "Failed")
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //calleventbus
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                //calleventbus
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });
                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
