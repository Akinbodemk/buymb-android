package com.partum.buymbmore.Models;

import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.EventModels.ElectricityEventModel;
import com.partum.buymbmore.EventModels.TVEventModel;
import com.partum.buymbmore.Network.PowerClient;
import com.partum.buymbmore.Network.SendMailClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Electricity {


    private String referenceCode,billerCode, serviceID,variationCode,phone;
    private int amount;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private String email;


    private String credentials =  "tns.ccare@gmail.com"+ ":" + "08169303901";
    // create Base64 encoded string
    private final String basicAuth =
            "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

    public Electricity(String referenceCode, String billerCode, String serviceID, String variationCode, String phone, int amount) {
        this.referenceCode = referenceCode;
        this.billerCode = billerCode;
        this.serviceID = serviceID;
        this.variationCode = variationCode;
        this.phone = phone;
        this.amount = amount;

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();
    }


    public Electricity(){

    }

    public void verifyBillerCode(){
        Call<ResponseBody> call = PowerClient.getInstance().getPowerAPI().verifyCode(basicAuth,billerCode,serviceID,variationCode);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject content = jsonObject.getJSONObject("content");
                        String name = content.getString("Customer_Name");
                        if(content.has("error")){
                            updateStatus("Failed");
                            EventBus.getDefault().post(new ElectricityEventModel(content.getString("error"),false));
                        }
                        else{
                            EventBus.getDefault().post(new ElectricityEventModel(name,true));
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        updateStatus("Failed");
                        EventBus.getDefault().post(new ElectricityEventModel(response.errorBody().string(),false));
                        // JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",response.errorBody().string());

                    }  catch (IOException e) {
                        EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                updateStatus("Failed");
                EventBus.getDefault().post(new ElectricityEventModel("Transaction Failed",false));
                Log.d("TV",t.getMessage());
            }
        });
    }

    public void payForPower(){
        Log.d("VT_PASS_REQUEST","reference: "+ referenceCode+ " service ID: "+ serviceID);
        Call<ResponseBody> call = PowerClient.getInstance().getPowerAPI().payForPower(basicAuth,referenceCode,serviceID,billerCode,variationCode,amount,phone);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        Log.d("VT_PASS_RESPONSE",jsonObject.toString());
                        JSONObject content = jsonObject.getJSONObject("content");
                        if(content.has("errors")){
                            updateStatus("Failed");
                            EventBus.getDefault().post(new ElectricityEventModel("Transaction Failed",false));
                        }
                        else {
                          JSONObject transactions = content.getJSONObject("transactions");
                          String status = transactions.getString("status");

                          if(status.equals("failed")){
                              updateStatus("Failed");
                              EventBus.getDefault().post(new ElectricityEventModel("Transaction Failed",false));
                          }
                          else{
                              updateStatus("Successful");
                              EventBus.getDefault().post(new ElectricityEventModel("Transaction Successful",true));
                              Wallet wallet = new Wallet("Debit",String.valueOf(amount));
                              wallet.applyChanges();
                              if(variationCode.equals("Prepaid")){
                                  String token = jsonObject.getString("tokem");
                                  sendEmail(email,token,serviceID);
                              }
                          }

                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        updateStatus("Failed");
                        EventBus.getDefault().post(new ElectricityEventModel("Transaction Failed",false));
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",errorObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                updateStatus("Failed");
                EventBus.getDefault().post(new ElectricityEventModel("Transaction Failed",false));
                Log.d("TV",t.getMessage());
            }
        });
    }


    private void updateStatus(String status){
        db.collection("Transactions")
                .whereEqualTo("reference",referenceCode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference dRef = document.getReference();
                                dRef
                                        .update("status", status)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //calleventbus
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                //calleventbus
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });
                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    private void sendEmail(String email, String token, String serviceID){
        Call<ResponseBody> call = SendMailClient.getInstance().getmailAPI().sendMail(token,serviceID,"Dear customer, here is your token",email);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){

                }

                else{
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",errorObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TV",t.getMessage());
            }
        });
    }
}
