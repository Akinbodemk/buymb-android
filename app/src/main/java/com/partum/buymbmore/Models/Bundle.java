package com.partum.buymbmore.Models;

public class Bundle {

    public String network;
    public String bundleName;
    public String bundleID;
    public String cardPrice;
    public String walletPrice;

    public Bundle(String network, String bundleName, String bundleID, String cardPrice, String walletPrice) {
        this.network = network;
        this.bundleName = bundleName;
        this.bundleID = bundleID;
        this.cardPrice = cardPrice;
        this.walletPrice = walletPrice;
    }

    public String getNetwork() {
        return network;
    }

    public String getBundleName() {
        return bundleName;
    }

    public String getBundleID() {
        return bundleID;
    }

    public String getCardPrice() {
        return cardPrice;
    }

    public String getWalletPrice() {
        return walletPrice;
    }
}
