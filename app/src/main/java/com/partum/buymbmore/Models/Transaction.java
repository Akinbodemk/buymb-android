package com.partum.buymbmore.Models;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

public class Transaction {
    private String name;
    private String clazz;
    private String status;
    private String type;
    private String amount;
    private String date;
    private String reference;
    private String channel;

    public Transaction(){

    }

    public Transaction(String name, String clazz, String status, String type, String amount, String date, String reference, String channel) {
        this.name = name;
        this.clazz = clazz;
        this.status = status;
        this.type = type;
        this.amount = amount;
        this.date = date;
        this.reference = reference;
        this.channel = channel;
    }


    public String getName() {
        return name;
    }

    public String getClazz() {
        return clazz;
    }

    public String getStatus() {
        return status;
    }

    public String getType() {
        return type;
    }

    public String getAmount() {
        return amount;
    }

    public String getDate() {
        return date;
    }

    public String getreference() {
        return reference;
    }

    public String getChannel() {
        return channel;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setClazz(String clazz) {
        this.clazz = clazz;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setAmount(String amount) {
        this.amount = amount;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public void setRefference(String refference) {
        this.reference = refference;
    }

    public void setChannel(String channel) {
        this.channel = channel;
    }

    public void uploadTransaction(){
        FirebaseAuth mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        String email = user.getEmail();
        FirebaseFirestore db = FirebaseFirestore.getInstance();
        Map<String, Object> transaction = new HashMap<>();
        transaction.put("Email", email);
        transaction.put("amount", amount);
        transaction.put("clazz", clazz);
        transaction.put("name",name);
        transaction.put("date",date);
        transaction.put("type",type);
        transaction.put("reference",reference);
        transaction.put("TimeStamp",new Date());
        transaction.put("status", status);
        transaction.put("Channel", channel);


        db.collection("Transactions").add(transaction)
                .addOnSuccessListener(new OnSuccessListener<DocumentReference>() {
                    @Override
                    public void onSuccess(DocumentReference documentReference) {
                        Log.d("TRANSACTIONS", "DocumentSnapshot written with ID: " + documentReference.getId());
                    }
                })
                .addOnFailureListener(new OnFailureListener() {
                    @Override
                    public void onFailure(@NonNull Exception e) {
                        Log.w("TRANSACTIONS", "Error adding document", e);
                    }
                });


    }
}
