package com.partum.buymbmore.Models;

import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Network.VerifyClient;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class Verifier {

    private String reference,email, clazz,amount, phone;
    private int networkID, bundleID;
    private FirebaseFirestore db;

    public Verifier(String reference, String email, String clazz, String amount){
        this.reference = reference;
        this.email = email;
        this.clazz = clazz;
        this.amount = amount;
        db = FirebaseFirestore.getInstance();
    }


    public  Verifier(String reference, String email, String clazz, String amount, int networkID, int bundleID, String phone ){
        this.phone = phone;
        this.bundleID = bundleID;
        this.networkID = networkID;
        this.amount = amount;
        this.clazz = clazz;
        this.email = email;
        this.reference = reference;
        db = FirebaseFirestore.getInstance();
    }

    public void verifyTransaction(){
        Call<ResponseBody> call = VerifyClient.getInstance().getTripsAPI().verifyTransaction("Bearer "+ "sk_test_256cf787bffc4a9daa21e3ed885201082c8521fb",reference);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject data = jsonObject.getJSONObject("data");
                        String status = data.getString("status");
                        if (status.equals("success")){
                            updateTransaction("Successful");
                        }
                        else{
                            updateTransaction("Failed");
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("VERIFIER",errorObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
               Log.d("VERIFIER",t.getMessage());
            }
        });
    }

    private void updateTransaction(String status){
        db.collection("Transactions")
                .whereEqualTo("Email",email ).whereEqualTo("reference",reference)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference referenze = document.getReference();
                                referenze
                                        .update("status", status)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");
                                               switch (clazz){
                                                   case "Funding":
                                                       Wallet wallet = new Wallet("credit", amount);
                                                       wallet.applyChanges();
                                                       break;
                                                   case "Data":
                                                       Data data = new Data(phone,networkID,bundleID,reference,Integer.valueOf(amount));
                                                       data.BuyData();
                                                       break;
                                                   default:
                                                       //should be unreachable
                                               }
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });

                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
