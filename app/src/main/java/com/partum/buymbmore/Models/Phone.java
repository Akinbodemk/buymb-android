package com.partum.buymbmore.Models;

public class Phone {

    String name, shortDescription, fullDescription, imageLink;
    int price ;


    public Phone() {
    }

    public Phone(String name, String shortDescription, String fullDescription, String imageLink, int price) {
        this.name = name;
        this.shortDescription = shortDescription;
        this.fullDescription = fullDescription;
        this.imageLink = imageLink;
        this.price = price;
    }


    public String getName() {
        return name;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public String getFullDescription() {
        return fullDescription;
    }

    public String getImageLink() {
        return imageLink;
    }

    public int getPrice() {
        return price;
    }
}


