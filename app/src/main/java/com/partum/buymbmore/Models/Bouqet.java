package com.partum.buymbmore.Models;

public class Bouqet {
    private String serviceID;
    private String name;
    private String variationCode;
    private String amount;

    public Bouqet(String serviceID, String name, String variationCode,String amount) {
        this.serviceID = serviceID;
        this.name = name;
        this.variationCode = variationCode;
        this.amount = amount;
    }

    public String getServiceID() {
        return serviceID;
    }

    public String getName() {
        return name;
    }

    public String getVariationCode() {
        return variationCode;
    }

    public String getAmount() {
        return amount;
    }
}
