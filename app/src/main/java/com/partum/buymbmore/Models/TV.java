package com.partum.buymbmore.Models;

import android.util.Base64;
import android.util.Log;

import androidx.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.EventModels.TVEventModel;
import com.partum.buymbmore.Network.TVClient;

import org.greenrobot.eventbus.EventBus;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TV {


    private String referenceCode,billerCode, serviceID,variationCode,phone;
    private int amount;
    private FirebaseFirestore db;
    private FirebaseAuth mAuth;
    private String email;


    private String credentials =  "oluwaseun909@gmail.com"+ ":" + "oluwaseunoflife";
    // create Base64 encoded string
    private final String basicAuth =
            "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);


    public TV (String referenceCode, String billerCode, String serviceID, String variationCode, String phone, int amount) {
        this.referenceCode = referenceCode;
        this.billerCode = billerCode;
        this.serviceID = serviceID;
        this.variationCode = variationCode;
        this.phone = phone;
        this.amount = amount;

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();
    }


    public TV(){

    }

    public void verifyBillerCode(){
        Call<ResponseBody> call = TVClient.getInstance().getTVAPI().verifyCode(basicAuth,billerCode,serviceID);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject content = jsonObject.getJSONObject("content");
                        String name = content.getString("Customer_Name");
                        if(content.has("error")){
                            EventBus.getDefault().post(new TVEventModel(content.getString("error"),false));
                        }
                        else{
                            EventBus.getDefault().post(new TVEventModel(name, true));
                            //payForTV(referenceCode,serviceID,billerCode,variationCode,phone);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        EventBus.getDefault().post(new TVEventModel(response.errorBody().string(),false));
                       // JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",response.errorBody().string());

                    }  catch (IOException e) {
                        EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                Log.d("TV",t.getMessage());
            }
        });
    }

    public void payForTV(){
        Call<ResponseBody> call = TVClient.getInstance().getTVAPI().payForTV(basicAuth,referenceCode,serviceID,billerCode,variationCode,phone);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject content = jsonObject.getJSONObject("content");
                        if(content.has("errors")){
                            updateStatus("Failed");
                            EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                        }
                        else {
                            updateStatus("Successful");
                            EventBus.getDefault().post(new TVEventModel("Transaction Successful",true));
                            Wallet wallet = new Wallet("Debit",String.valueOf(amount));
                            wallet.applyChanges();
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {
                        updateStatus("Failed");
                        EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",errorObject.toString());

                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                updateStatus("Failed");
                EventBus.getDefault().post(new TVEventModel("Transaction Failed",false));
                Log.d("TV",t.getMessage());
            }
        });
    }

    private void updateStatus(String status){
        db.collection("Transactions")
                .whereEqualTo("reference",referenceCode)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference dRef = document.getReference();
                                dRef
                                        .update("status", status)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                //calleventbus
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");
                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                //calleventbus
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });
                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
