package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.viewpager.widget.ViewPager;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.widget.ImageView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.tabs.TabLayout;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Adapters.NetworkAdapter;
import com.partum.buymbmore.Adapters.TabAdapter;
import com.partum.buymbmore.Fragments.AirtimeFragment;
import com.partum.buymbmore.Fragments.DataFragment;

import java.util.ArrayList;

public class BuyDataAndAirtime extends AppCompatActivity {

    private ImageView networkImage,bundleImage;
    private ArrayList<String> networks = new ArrayList<String>();
    private NetworkAdapter networkAdapter;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter adapter;
    private ImageView back;
    private TabAdapter tabAdapter;
    private TabLayout tabLayout;
    private ViewPager viewPager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_data_and_airtime);

        back = findViewById(R.id.back_to_home_from_buy_data);

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        networks.add("Airtel");
        networks.add("MTN");
        networks.add("9 Mobile");
        networks.add("Glo");


        viewPager = (ViewPager) findViewById(R.id.viewPager);
        tabLayout = (TabLayout) findViewById(R.id.tabLayout);
        tabAdapter = new TabAdapter(getSupportFragmentManager());
        tabAdapter.addFragment(new DataFragment(), "Data");
        tabAdapter.addFragment(new AirtimeFragment(), "Airtime");
        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                if(position == 1){

                }
                else if(position == 2){

                }

            }
            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });
        viewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(viewPager);




    }





}
