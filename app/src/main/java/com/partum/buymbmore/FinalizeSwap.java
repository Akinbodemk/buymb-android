package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

public class FinalizeSwap extends AppCompatActivity {

    private TextView ussdCode;
    private Button done;
    private RelativeLayout copyCode;
    private ImageView back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_finalize_swap);

        ussdCode = findViewById(R.id.ussd_code);
        copyCode = findViewById(R.id.copy_code);
        done = findViewById(R.id.swap_done);
        back = findViewById(R.id.back_from_finalize_swap);


        back.setOnClickListener(v -> {
            onBackPressed();
        });


        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        if (bundle != null){
            String code = bundle.getString("CODE");

            ussdCode.setText(code);


        }

        done.setOnClickListener(v -> {
            openWhatsapp("https://wa.me/+2348140011580");
        });

        copyCode.setOnClickListener(v -> {
            copyUserCode();
        });
    }

    public void openWhatsapp(String url){

        PackageManager packageManager = this.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

    private void copyUserCode(){
        ClipboardManager clipboard = (ClipboardManager)
                getSystemService(Context.CLIPBOARD_SERVICE);
        ClipData clip = ClipData.newPlainText("User Code", ussdCode.getText());
        clipboard.setPrimaryClip(clip);
        Toast.makeText(FinalizeSwap.this,"Copied to clipboard!",Toast.LENGTH_LONG).show();
    }

}
