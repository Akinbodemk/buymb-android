package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class SelectPaymentMode extends AppCompatActivity {

    private ImageView back;
    private RelativeLayout agent, card;
    private String mode, networkName,bundleName,phoneNumber,amount;
    private int networkID, bundleID;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_select_payment_mode);

        back = findViewById(R.id.back_from_payment_method);
        agent = findViewById(R.id.buymb_agent);
        card = findViewById(R.id.card);

        back.setOnClickListener( v -> {
            onBackPressed();
        });


        agent.setOnClickListener( v -> {
            openWhatsapp("https://wa.me/+2348140011580");
        });

        card.setOnClickListener(v -> {
            Intent intent = new Intent(SelectPaymentMode.this,PaymentActivity.class);
            intent.putExtra("MODE","Funding");
            startActivity(intent);
        });

    }

    public void openWhatsapp(String url){
        PackageManager packageManager = this.getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }
}
