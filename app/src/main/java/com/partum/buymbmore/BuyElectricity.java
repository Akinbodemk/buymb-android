package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.EventModels.ElectricityEventModel;
import com.partum.buymbmore.Models.Electricity;
import com.partum.buymbmore.Models.Transaction;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;

public class BuyElectricity extends AppCompatActivity {

    private Spinner providers,plans;
    private EditText meterNumber, amountToPay,phoneED;
    private ImageView back;
    private Button proceed,done;
    private AVLoadingIndicatorView loadingIndicatorView;
    private String transactionreference;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
    String date = simpleDateFormat.format(new Date());
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    private String[] powerProviders = { "IKEDC", "EKEDC", "IBEDC"};
    private String[] powerPlans = {"Postpaid", "Prepaid"};
    private RelativeLayout powerSuccessLayout,poweranimationLayout;
    private TextView customerName;
    private Electricity elc;


    private String tRef,billersCode,serviceID,variationCode,amount,phoneNumber;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_electricity);

        back = findViewById(R.id.back_from_buy_electricity);
        providers = findViewById(R.id.providers);
        plans = findViewById(R.id.plans);
        meterNumber = findViewById(R.id.meter_number);
        amountToPay = findViewById(R.id.electricity_amount);
        proceed = findViewById(R.id.proceed_electricity);
        loadingIndicatorView = findViewById(R.id.pay_progress_electricity);
        phoneED = findViewById(R.id.phone_number_electricity);
        poweranimationLayout = findViewById(R.id.power_animation_layout);
        powerSuccessLayout = findViewById(R.id.power_success_layout);
        done = findViewById(R.id.ok_for_power);
        customerName = findViewById(R.id.electricity_customer_name);

        tRef = randomAlphaNumeric(12);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();


        back.setOnClickListener(v -> {
            onBackPressed();
        });

        ArrayAdapter<String> adapter = new ArrayAdapter<>(BuyElectricity.this, R.layout.spinner_item, powerProviders);
        ArrayAdapter<String> adapter2 = new ArrayAdapter<>(BuyElectricity.this, R.layout.spinner_item, powerPlans);
        adapter.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);
        adapter2.setDropDownViewResource(R.layout.support_simple_spinner_dropdown_item);

        providers.setAdapter(adapter);
        plans.setAdapter(adapter2);
        serviceID = getServiceID("IKEDC");
        variationCode = "Postpaid";

        providers.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                serviceID = getServiceID(powerProviders[position]);

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        plans.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                variationCode = powerPlans[position];

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        proceed.setOnClickListener(v -> {
            if(!TextUtils.isEmpty(meterNumber.getText()) || !TextUtils.isEmpty(amountToPay.getText()) || !TextUtils.isEmpty(phoneED.getText())){
                   billersCode = meterNumber.getText().toString();
                   amount = amountToPay.getText().toString();
                   phoneNumber = phoneED.getText().toString();

                final EditText taskEditText = new EditText(this);
                taskEditText.setPadding(30,0,0,20);
                taskEditText.setSingleLine();
                taskEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                FrameLayout container = new FrameLayout(this);
                FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.leftMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                taskEditText.setLayoutParams(params);
                container.addView(taskEditText);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Wallet Pin")
                        .setMessage("Please enter your wallet Pin")
                        .setView(container)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(!TextUtils.isEmpty(taskEditText.getText())){
                                    String pin = String.valueOf(taskEditText.getText());
                                    dialog.dismiss();
                                    checkPinAndBalance(Integer.valueOf(amount), pin);
                                     elc = new Electricity(tRef,billersCode,serviceID,variationCode,phoneNumber, Integer.valueOf(amount));
                                    poweranimationLayout.setVisibility(View.VISIBLE);

                                }
                                else{
                                    Toast.makeText(BuyElectricity.this,"Please Enter a valid pin",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();

            }
            else {
                Toast.makeText(BuyElectricity.this," Please fill in all details",Toast.LENGTH_SHORT).show();
            }
        });



    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    private String getServiceID(String providerName){
        switch (providerName){
            case "IKEDC":
                return "ikeja-electric";
            case "IBEDC":
                return  "ibadan-electric";
            default:
                return "eko-electric";
        }
    }

    public void checkPinAndBalance(int priceToPay, String pin){
        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                //String lastname = (String) document.get(getString(R.string.lastNameField));
                                long walletBalance = (long) document.get(getString(R.string.walletBalanceField));
                                String walletPin = (String) document.get(getString(R.string.walletPinField));
                                if(walletPin.equals(pin)){
                                    if(walletBalance >= priceToPay){
                                        Transaction transaction = new Transaction("Electricity bills","Power","Pending","debit",amount,
                                                date,tRef,"Wallet");
                                        transaction.uploadTransaction();
                                        elc.verifyBillerCode();
                                    }
                                    else{
                                        poweranimationLayout.setVisibility(View.GONE);
                                        proceed.setVisibility(View.VISIBLE);
                                        Toast.makeText(BuyElectricity.this,"Insufficient Funds",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    poweranimationLayout.setVisibility(View.GONE);
                                    proceed.setVisibility(View.VISIBLE);
                                    Toast.makeText(BuyElectricity.this,"Wrong Pin",Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Log.d("HOME", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }


    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(ElectricityEventModel event){
        if(event.getState()){
            if(event.getMessage().equals("Transaction Successful")){
                Toast.makeText(BuyElectricity.this,"Transaction Successful",Toast.LENGTH_SHORT).show();
                poweranimationLayout.setVisibility(View.GONE);
                powerSuccessLayout.setVisibility(View.VISIBLE);
                done.setOnClickListener(v -> {
                    startActivity(new Intent(BuyElectricity.this,HomeActivity.class));
                });
            }
            else {
                poweranimationLayout.setVisibility(View.GONE);
                customerName.setVisibility(View.VISIBLE);
                customerName.setText("Name: "+event.getMessage());

                proceed.setText("Confirm Order");
                proceed.setOnClickListener( v -> {
                    poweranimationLayout.setVisibility(View.VISIBLE);
                    elc.payForPower();
                });
            }



        }else{
            proceed.setVisibility(View.VISIBLE);
            poweranimationLayout.setVisibility(View.GONE);

            Toast.makeText(BuyElectricity.this,"Energy Purchase Failed",Toast.LENGTH_SHORT).show();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


}
