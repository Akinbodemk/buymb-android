package com.partum.buymbmore.Fragments;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.partum.buymbmore.R;
import com.partum.buymbmore.SelectBundleActivity;

public class DataFragment extends Fragment {

    private LinearLayout mtn,airtel,glo, nine_mobile;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        View mview = inflater.inflate(R.layout.buydatafragment,container,false);

        mtn = mview.findViewById(R.id.select_mtn);
        airtel = mview.findViewById(R.id.select_airtel);
        glo = mview.findViewById(R.id.select_glo);
        nine_mobile = mview.findViewById(R.id.select_nine_mobile);

        mtn.setOnClickListener(v -> {
            Intent intent = new Intent(getActivity(), SelectBundleActivity.class);
            intent.putExtra("NETWORK_NAME","MTN");
            intent.putExtra("NETWORK_ID",1);
            startActivity(intent);
        });

        airtel.setOnClickListener( v -> {
            Intent intent = new Intent(getActivity(),SelectBundleActivity.class);
            intent.putExtra("NETWORK_NAME", "Airtel");
            intent.putExtra("NETWORK_ID",2);
            startActivity(intent);
        });

        glo.setOnClickListener( v -> {
            Intent intent = new Intent(getActivity(),SelectBundleActivity.class);
            intent.putExtra("NETWORK_NAME", "Glo");
            intent.putExtra("NETWORK_ID",4);
            startActivity(intent);
        });

        nine_mobile.setOnClickListener( v -> {
            Intent intent = new Intent(getActivity(),SelectBundleActivity.class);
            intent.putExtra("NETWORK_NAME", "9 Mobile");
            intent.putExtra("NETWORK_ID",3);
            startActivity(intent);
        });





        return mview;
    }
}
