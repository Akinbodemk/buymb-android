package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.RelativeLayout;

public class OtherServices extends AppCompatActivity {

    private ImageView back;
    private RelativeLayout pos,cac;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_other_services);

        back = findViewById(R.id.back_from_other_services);
        pos = findViewById(R.id.pos);
        cac = findViewById(R.id.cac);


        back.setOnClickListener(v -> {
            onBackPressed();
        });


        pos.setOnClickListener( v ->{
            composeEmail("services@Buymb.com","REQUEST FOR POS","Hello i would like to request for a pos device for my business");

        });

        cac.setOnClickListener( v ->{
            composeEmail("services@Buymb.com","REQUEST FOR CAC","Hello i would like to initiate CAC registration for my business for my business");

        });
    }



    public void composeEmail(String address, String subject,String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:support@hubryde.com")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT,message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
