package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.wang.avi.AVLoadingIndicatorView;

import java.util.HashMap;
import java.util.Map;

public class EditProfile extends AppCompatActivity {


    private EditText firstnameED,lastnameED,phonenumberED,pinED;
    private Button edit;
    private ImageView back;
    private AVLoadingIndicatorView loading;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_edit_profile);

        back = findViewById(R.id.back_from_edit_profile);
        firstnameED = findViewById(R.id.edit_firstname);
        lastnameED = findViewById(R.id.edit_lastname);
        phonenumberED = findViewById(R.id.edit_phone_number);
        pinED = findViewById(R.id.edit_wallet_pin);
        edit = findViewById(R.id.edit_profile);
        loading = findViewById(R.id.edit_progress);
        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        fetchDetails();


        edit.setOnClickListener(v -> {
            edit.setVisibility(View.GONE);
            loading.setVisibility(View.VISIBLE);
            editProfile();
        });
    }

    private void fetchDetails(){
       /* db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                String lastname = (String) document.get(getString(R.string.lastNameField));
                                String firstname = (String) document.get(getString(R.string.firstNameField));
                                String phone = (String) document.get(getString(R.string.phoneField));
                                String walletPin = (String) document.get(getString(R.string.walletPinField));
                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        firstnameED.setText(firstname);
                                        lastnameED.setText((lastname));
                                        phonenumberED.setText(phone);
                                        pinED.setText(walletPin);

                                    }
                                });
                            }
                        } else {
                            Log.d("HOME", "Error getting documents: ", task.getException());
                        }
                    }
                });*/
    }

    private void editProfile(){

        Map<String, Object> user = new HashMap<>();
        //user.put(getString(R.string.firstNameField), firstnameED.getText().toString());
        //user.put(getString(R.string.lastNameField), lastnameED.getText().toString());
        user.put(getString(R.string.phoneField), phonenumberED.getText().toString());
        user.put(getString(R.string.walletPinField),pinED.getText().toString());

        db.collection("users")
                .whereEqualTo("Email",email)
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                DocumentReference reference = document.getReference();
                                reference
                                        .update(user)
                                        .addOnSuccessListener(new OnSuccessListener<Void>() {
                                            @Override
                                            public void onSuccess(Void aVoid) {
                                                edit.setVisibility(View.VISIBLE);
                                                loading.setVisibility(View.GONE);
                                                Log.d("VERIFIER", "DocumentSnapshot successfully updated!");

                                            }
                                        })
                                        .addOnFailureListener(new OnFailureListener() {
                                            @Override
                                            public void onFailure(@NonNull Exception e) {
                                                Log.w("VERIFIER", "Error updating document", e);
                                            }
                                        });
                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }
}
