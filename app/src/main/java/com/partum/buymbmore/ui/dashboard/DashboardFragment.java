package com.partum.buymbmore.ui.dashboard;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProviders;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Models.Transaction;
import com.partum.buymbmore.R;
import com.partum.buymbmore.TransactionDetails;
import com.partum.buymbmore.TransactionsList;

public class DashboardFragment extends Fragment {

    private SwipeRefreshLayout referesh;
    private RecyclerView transactionsRecycler;
    private TextView noTransactions;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter adapter;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.history, container, false);

        referesh = root.findViewById(R.id.swipe_container);
        transactionsRecycler = root.findViewById(R.id.history_recycler);
        noTransactions = root.findViewById(R.id.no_transactions_yet);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();


        Query transactions  = db.collection(getString(R.string.transaction_documents)).whereEqualTo("Email", email)
                .orderBy("TimeStamp", Query.Direction.DESCENDING);
        transactions.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(!(queryDocumentSnapshots == null)){
                    noTransactions.setVisibility(View.GONE);
                    transactionsRecycler.setVisibility(View.VISIBLE);
                }
            }
        });

        FirestoreRecyclerOptions<Transaction> options = new FirestoreRecyclerOptions.Builder<Transaction>()
                .setQuery(transactions, Transaction.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Transaction, DashboardFragment.TransactionHolder>(options) {
            @Override
            public void onBindViewHolder(DashboardFragment.TransactionHolder holder, int position, Transaction model) {
                holder.name.setText(model.getName());
                holder.date.setText(model.getDate());

                if (!model.getType().equals("credit")) {
                    holder.clazz.setImageResource(R.drawable.ic_purple_circle);
                }

                if (model.getStatus().equals("Pending")){
                    holder.status.setText("Pending");
                    holder.status.setTextColor(Color.parseColor( "#E76E51"));
                }
                else if (model.getStatus().equals("Failed")){
                    holder.status.setText("Failed");
                    holder.status.setTextColor(Color.RED);
                }
                else {
                    holder.status.setText("Successful");
                    holder.status.setTextColor(Color.parseColor("#68C25F"));
                }


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(getActivity(), TransactionDetails.class);
                        intent.putExtra("TRANSACTION_NAME",model.getName());
                        intent.putExtra("TRANSACTION_CLASS",model.getClazz());
                        intent.putExtra("TRANSACTION_AMOUNT",model.getAmount());
                        intent.putExtra("TRANSACTION_TYPE",model.getType());
                        intent.putExtra("TRANSACTION_STATUS",model.getStatus());
                        intent.putExtra("TRANSACTION_DATE",model.getDate());
                        intent.putExtra("TRANSACTION_CHANNEL",model.getChannel());
                        intent.putExtra("TRANSACTION_REFERENCE",model.getreference());
                        startActivity(intent);
                    }
                });


            }

            @Override
            public DashboardFragment.TransactionHolder onCreateViewHolder(ViewGroup group, int i) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.transaction, group, false);

                return new DashboardFragment.TransactionHolder(view);
            }
        };

        transactionsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));
        transactionsRecycler.setAdapter(adapter);





        return root;
    }


    public class TransactionHolder extends RecyclerView.ViewHolder {
        private TextView status, amount,name,date;
        private ImageView sign, clazz;

        public TransactionHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.transaction_status);
            name = itemView.findViewById(R.id.transactionName);
            date = itemView.findViewById(R.id.transaction_date);
            clazz = itemView.findViewById(R.id.transaction_class);
        }
    }


    @Override
    public void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    public void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}

