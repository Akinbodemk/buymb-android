package com.partum.buymbmore.ui.home;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.Nullable;
import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Adapters.HomeAdapter;
import com.partum.buymbmore.AirtimeToCash;
import com.partum.buymbmore.BuyDataAndAirtime;
import com.partum.buymbmore.BuyPhone;
import com.partum.buymbmore.OtherServices;
import com.partum.buymbmore.PaymentActivity;
import com.partum.buymbmore.R;
import com.partum.buymbmore.Utility;
import com.partum.buymbmore.SelectBundleActivity;

public class HomeFragment extends Fragment {

    private RecyclerView homeRecycler;
    private HomeAdapter adapter;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    private TextView balance,welcomeText;
    private ImageView eye;
    private boolean balanceIsVisible;
    private  String currentBalance;
    private RelativeLayout fundWalletScreen;
    private LinearLayout card,agent;
    private RelativeLayout fundWallet;

    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.home_activity, container, false);

        adapter = new HomeAdapter();
        homeRecycler = root.findViewById(R.id.home_recycler);
        balance = root.findViewById(R.id.balance_text);
        welcomeText = root.findViewById(R.id.welcome_text);
        fundWalletScreen = root.findViewById(R.id.fund_wallet_layout);
        card = root.findViewById(R.id.bank_account);
        agent = root.findViewById(R.id.buymb_agent);
        eye = root.findViewById(R.id.eye);
        fundWallet = root.findViewById(R.id.fund);

        homeRecycler.setHasFixedSize(true);

        GridLayoutManager glm = new GridLayoutManager(getContext(), 4);

        homeRecycler.setLayoutManager(glm);

        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                int position = homeRecycler.indexOfChild(view);
                switch (position){
                    case 0:
                        startActivity(new Intent(getActivity(), BuyDataAndAirtime.class));
                        break;
                    case 1:
                        startActivity(new Intent(getActivity(), Utility.class));
                        break;
                    case 2:
                        startActivity(new Intent(getActivity(), AirtimeToCash.class));
                        break;
                    case 3:
                        openWhatsapp("https://wa.me/+2348140011580");
                        break;
                    case 4:
                        startActivity(new Intent(getActivity(), AirtimeToCash.class));
                        break;
                    case 5:
                        startActivity(new Intent(getActivity(), BuyPhone.class));
                        break;
                    case 6:
                        startActivity(new Intent(getActivity(), OtherServices.class));
                        break;
                    default:
                        System.out.println("Not Reachable");

                }
            }
        });
        homeRecycler.setAdapter(adapter);



        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("WALLET", document.getId() + " => " + document.getData());
                                DocumentReference reference = document.getReference();
                                reference.addSnapshotListener(new EventListener<DocumentSnapshot>() {
                                    @Override
                                    public void onEvent(@Nullable DocumentSnapshot snapshot,
                                                        @Nullable FirebaseFirestoreException e) {
                                        if (e != null) {
                                            Log.w("WALLET", "Listen failed.", e);
                                            return;
                                        }
                                        if (snapshot != null && snapshot.exists()) {
                                            long wallet_balance = (long) document.get(getString(R.string.walletBalanceField));
                                            getActivity().runOnUiThread(new Runnable() {
                                                @Override
                                                public void run() {
                                                    balance.setText("₦"+wallet_balance);
                                                }
                                            });
                                        } else {
                                            Log.d("WALLET", "Current data: null");
                                        }
                                    }
                                });

                            }
                        } else {
                            Log.d("WALLET", "Error getting documents: ", task.getException());
                        }
                    }
                });

        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                String name = (String) document.get(getString(R.string.fullnameField));
                                getActivity().runOnUiThread(new Runnable() {
                                    @Override
                                            public void run() {
                                            welcomeText.setText("Welcome, "+name);
                                }
                            });
                        }
                    } else {
                        Log.d("HOME", "Error getting documents: ", task.getException());
                    }
                }
    });

     fundWallet.setOnClickListener(v -> {
         fundWalletScreen.setVisibility(View.VISIBLE);
         card.setOnClickListener( d -> {
             Intent intent = new Intent(getActivity(), PaymentActivity.class);
             intent.putExtra("MODE","Funding");
             startActivity(intent);
             fundWalletScreen.setVisibility(View.GONE);
         });

         agent.setOnClickListener(b -> {
             fundWalletScreen.setVisibility(View.GONE);
             openWhatsapp("https://wa.me/+2348140011580");
         });
     });


     fundWalletScreen.setOnClickListener(v -> {
         fundWalletScreen.setVisibility(View.GONE);
     });

        return root;
    }

    public void openWhatsapp(String url){
        PackageManager packageManager = getActivity().getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }


}