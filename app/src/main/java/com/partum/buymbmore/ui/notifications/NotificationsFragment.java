package com.partum.buymbmore.ui.notifications;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.partum.buymbmore.Adapters.ProfileAdapter;
import com.partum.buymbmore.EditProfile;
import com.partum.buymbmore.R;
import com.partum.buymbmore.Startup.RootPage;

public class NotificationsFragment extends Fragment {

    private RecyclerView itemsRecycler;
    private ProfileAdapter adapter;
    private FirebaseAuth mAuth;
    private AlertDialog.Builder alertdialogbuilder;
    private AlertDialog alertDialog;



    public View onCreateView(@NonNull LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {

        View root = inflater.inflate(R.layout.more_activity, container, false);

        itemsRecycler = root.findViewById(R.id.more_items_recycler);

        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();

        adapter = new ProfileAdapter();

        itemsRecycler.setHasFixedSize(true);
        itemsRecycler.setLayoutManager(new LinearLayoutManager(getContext()));

        adapter.setClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = itemsRecycler.indexOfChild(v);
                switch (position){
                    case 0:
                        startActivity(new Intent(getActivity(), EditProfile.class));
                        break;
                    case 1:
                        openWhatsapp("https://wa.me/+2348140011580");
                        break;
                    case 2:
                        openWhatsapp("https://wa.me/+2348140011580");
                        break;
                    case 3:
                        openWhatsapp("https://wa.me/+2348140011580");
                        break;
                    case 4:
                        showAlertDialog();
                        break;
                    default:
                        System.out.println("Not Reachable");

                }

            }
        });

        itemsRecycler.setAdapter(adapter);


        return root;
    }

    private void showAlertDialog(){
        alertdialogbuilder = new AlertDialog.Builder(getContext());
        alertdialogbuilder.setTitle("Logout");
        alertdialogbuilder.setMessage("Are you sure you want to logout?");
        alertdialogbuilder.setCancelable(true);
        alertdialogbuilder.setPositiveButton("Sure", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                logout();
            }
        });
        alertdialogbuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                alertDialog.dismiss();
            }
        });
        alertDialog = alertdialogbuilder.create();
        alertDialog.show();
    }

    private void logout(){
        Intent intent = new Intent(getActivity(), RootPage.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
        getActivity().finish();
        FirebaseAuth.getInstance().signOut();
    }

    public void openWhatsapp(String url){
        PackageManager packageManager = getActivity().getPackageManager();
        Intent i = new Intent(Intent.ACTION_VIEW);
        try {
            i.setPackage("com.whatsapp");
            i.setData(Uri.parse(url));
            if (i.resolveActivity(packageManager) != null) {
                this.startActivity(i);
            }
        } catch (Exception e){
            e.printStackTrace();
        }
    }

}