package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.DocumentReference;
import com.google.firebase.firestore.DocumentSnapshot;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.EventModels.DataEventModel;
import com.partum.buymbmore.Models.Data;
import com.partum.buymbmore.Models.Transaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

public class SelectBundleActivity extends AppCompatActivity {


    private ImageView networkIcon;
    private TextView networkName;
    private Spinner bundleSpinner;
    private EditText phoneNumber;
    private Button startBuyData,ok;
    private FirebaseFirestore db;
    private String email;
    private ImageView back;
    private FirebaseAuth mAuth;
    private ArrayList<Integer> ids,walletPrices,cardPrices;
    private ArrayList<String> bundleNames;
    private ArrayList<com.partum.buymbmore.Models.Bundle> bundles = new ArrayList<>();
    private RelativeLayout optionsLayout,animationLayout,successLayout;
    private LinearLayout payWithCard, payWithWallet;
    private String bundleID,transactionReference,cardPrice,bundleName,phone,walletPrice,network_name;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
    String date = simpleDateFormat.format(new Date());
    private int networkID;
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.select_bundle_activity);

        networkIcon = findViewById(R.id.data_network_image);
        networkName = findViewById(R.id.data_network_name);
        bundleSpinner = findViewById(R.id.bundle_spinner);
        phoneNumber = findViewById(R.id.user_phone_number);
        startBuyData = findViewById(R.id.start_buy_data);
        back = findViewById(R.id.back_to_home_from_selectBundle);
        optionsLayout = findViewById(R.id.buy_bundle_layout);
        payWithCard = findViewById(R.id.pay_with_card);
        payWithWallet = findViewById(R.id.pay_with_wallet);
        successLayout = findViewById(R.id.success_layout);
        ok = findViewById(R.id.ok_for_wallet);
        animationLayout = findViewById(R.id.payment_option_animation_layout);


        transactionReference = randomAlphaNumeric(12);




        back.setOnClickListener( v -> {
            onBackPressed();
        });

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();




       Intent intent = getIntent();

       Bundle bundle = intent.getExtras();

       if (bundle != null){
           network_name = bundle.getString("NETWORK_NAME");
           networkID = bundle.getInt("NETWORK_ID");

           networkName.setText(network_name + " Data");
           setNetworkIcon(network_name);
           findBundles(network_name);

           bundleSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
               @Override
               public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                   if(!bundles.isEmpty()){
                       com.partum.buymbmore.Models.Bundle currentBundle = bundles.get(position);
                       bundleName = currentBundle.getBundleName();
                       bundleID = currentBundle.getBundleID();
                       cardPrice = currentBundle.getCardPrice();
                       walletPrice = currentBundle.getWalletPrice();
                   }

               }

               @Override
               public void onNothingSelected(AdapterView<?> parent) {

               }
           });

           startBuyData.setOnClickListener(v -> {
               optionsLayout.setVisibility(View.VISIBLE);
           });

           payWithCard.setOnClickListener(v -> {
               if(!TextUtils.isEmpty(phoneNumber.getText())){
                   phone = phoneNumber.getText().toString();
                   Intent intent1 = new Intent(SelectBundleActivity.this, PaymentActivity.class);
                   intent1.putExtra("MODE","Data");
                   intent1.putExtra("NETWORK_NAME",network_name);
                   intent1.putExtra("BUNDLE_NAME",bundleName);
                   intent1.putExtra("CARD_AMOUNT",cardPrice);
                   intent1.putExtra("REFERENCE",transactionReference);
                   intent1.putExtra("PHONE_NUMBER",phone);
                   intent1.putExtra("BUNDLE_ID",Integer.valueOf(bundleID));
                   intent1.putExtra("NETWORK_ID",networkID);
                   startActivity(intent1);
                   optionsLayout.setVisibility(View.GONE);
               }
               else{
                   Toast.makeText(SelectBundleActivity.this, "Phone Number can not be empty",Toast.LENGTH_SHORT).show();
               }
           });

           payWithWallet.setOnClickListener(v -> {
               optionsLayout.setVisibility(View.GONE);
               if(!TextUtils.isEmpty(phoneNumber.getText())){
                   phone = phoneNumber.getText().toString();
                   final EditText taskEditText = new EditText(this);
                   taskEditText.setPadding(30,0,0,20);
                   taskEditText.setSingleLine();
                   taskEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                   FrameLayout container = new FrameLayout(this);
                   FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                   params.leftMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                   params.rightMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                   taskEditText.setLayoutParams(params);
                   container.addView(taskEditText);
                   AlertDialog dialog = new AlertDialog.Builder(this)
                           .setTitle("Wallet Pin")
                           .setMessage("Please enter your wallet Pin")
                           .setView(container)
                           .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                               @Override
                               public void onClick(DialogInterface dialog, int which) {

                                   if(!TextUtils.isEmpty(taskEditText.getText())){
                                       String pin = String.valueOf(taskEditText.getText());
                                       dialog.dismiss();
                                       animationLayout.setVisibility(View.VISIBLE);
                                       checkPinAndBalance(Integer.valueOf(walletPrice), pin);

                                   }
                                   else{
                                       Toast.makeText(SelectBundleActivity.this,"Please Enter a valid pin",Toast.LENGTH_SHORT).show();
                                   }
                               }
                           })
                           .setNegativeButton("Cancel", null)
                           .create();
                   dialog.show();
               }
               else{
                   Toast.makeText(SelectBundleActivity.this, "Phone Number can not be empty",Toast.LENGTH_SHORT).show();
               }
           });
       }






    }

    private void findBundles(String networkName){
        db.collection("Bundles").whereEqualTo("Network", networkName).get().addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
            @Override
            public void onComplete(@NonNull Task<QuerySnapshot> task) {
                if (task.isSuccessful()) {
                    for (QueryDocumentSnapshot document : task.getResult()) {
                        Log.d("DATA", document.getId() + " => " + document.getData());
                        ids = (ArrayList<Integer>) document.get("IDs");
                        cardPrices = (ArrayList<Integer>) document.get("cardPrices");
                        walletPrices = (ArrayList<Integer>) document.get("wallet");
                        bundleNames = (ArrayList<String>) document.get("bundle");
                        for(int i = 0; i <= ids.size()-1; i++){
                            com.partum.buymbmore.Models.Bundle bundle = new com.partum.buymbmore.Models.Bundle(networkName,
                                    bundleNames.get(i),String.valueOf(ids.get(i)),String.valueOf(cardPrices.get(i)),String.valueOf(walletPrices.get(i))
                            );
                            Log.d("BUNDLE","networkname: "+bundle.getNetwork());
                            Log.d("BUNDLE","bundleName: "+bundle.getBundleName());
                            Log.d("BUNDLE","id: "+bundle.getBundleID());
                            Log.d("BUNDLE","card: "+bundle.getCardPrice());
                            bundles.add(bundle);
                        }
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(SelectBundleActivity.this, android.R.layout.simple_spinner_item, bundleNames);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        bundleSpinner.setAdapter(adapter);

                    }
                } else {
                    Log.d("DATA", "Error getting documents: ", task.getException());
                }
            }
        });

    }



    private void setNetworkIcon(String name){
        switch (name){
            case "MTN":
               networkIcon.setImageResource(R.drawable.mtn_logo);
               break;
            case "Airtel":
                networkIcon.setImageResource(R.drawable.airtel_logo);
                break;
            case "Glo":
                networkIcon.setImageResource(R.drawable.glo_logo);
            default:
                networkIcon.setImageResource(R.drawable.nine_mobile);

        }
    }

    public void checkPinAndBalance(int priceToPay, String pin){
        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                //String lastname = (String) document.get(getString(R.string.lastNameField));
                                long walletBalance = (long) document.get(getString(R.string.walletBalanceField));
                                String walletPin = (String) document.get(getString(R.string.walletPinField));
                                if(walletPin.equals(pin)){
                                    if(walletBalance >= priceToPay){
                                        Transaction transaction = new Transaction(networkName+" "+bundleName,"Data","Pending","debit",walletPrice,
                                                date,transactionReference,"Wallet");
                                        transaction.uploadTransaction();
                                        Data data = new Data(phone,networkID,Integer.valueOf(bundleID),transactionReference,Integer.valueOf(walletPrice));
                                        data.BuyData();
                                    }
                                    else{
                                        animationLayout.setVisibility(View.GONE);
                                        Toast.makeText(SelectBundleActivity.this,"Insufficient Funds",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    Toast.makeText(SelectBundleActivity.this,"Wrong Pin",Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Log.d("HOME", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DataEventModel event){
        if(event.getState()){
            animationLayout.setVisibility(View.GONE);
            successLayout.setVisibility(View.VISIBLE);
            ok.setOnClickListener(v -> {
                startActivity(new Intent(SelectBundleActivity.this,HomeActivity.class));
            });
        }else{
            Toast.makeText(SelectBundleActivity.this,"Data Purchase Failed",Toast.LENGTH_SHORT).show();
            animationLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


}


