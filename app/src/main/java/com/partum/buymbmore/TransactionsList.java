package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Models.Transaction;

public class TransactionsList extends AppCompatActivity {

    private ImageView back;
    private RecyclerView transactionsRecycler;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    private FirestoreRecyclerAdapter adapter;
    private TextView noTransactions;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transactions_list);

        back = findViewById(R.id.back_from_transactions);
        transactionsRecycler = findViewById(R.id.all_transactions_recycler);
        noTransactions = findViewById(R.id.no_transactions3);

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();


        Query transactions  = db.collection(getString(R.string.transaction_documents)).whereEqualTo("Email", email)
                .orderBy("TimeStamp", Query.Direction.DESCENDING);
        transactions.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(!(queryDocumentSnapshots == null)){
                    noTransactions.setVisibility(View.GONE);
                    transactionsRecycler.setVisibility(View.VISIBLE);
                }
            }
        });

        FirestoreRecyclerOptions<Transaction> options = new FirestoreRecyclerOptions.Builder<Transaction>()
                .setQuery(transactions, Transaction.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Transaction, TransactionsList.TransactionHolder>(options) {
            @Override
            public void onBindViewHolder(TransactionsList.TransactionHolder holder, int position, Transaction model) {
                holder.name.setText(model.getName());
                holder.date.setText(model.getDate());

                if (!model.getType().equals("credit")) {
                    holder.clazz.setImageResource(R.drawable.ic_lightbulb);
                }

                if (model.getStatus().equals("Pending")){
                    holder.status.setText("Pending");
                    holder.status.setTextColor(Color.parseColor( "#E76E51"));
                }
                else if (model.getStatus().equals("Failed")){
                    holder.status.setText("Failed");
                    holder.status.setTextColor(Color.RED);
                }
                else {
                    holder.status.setText("Successful");
                    holder.status.setTextColor(Color.parseColor("#68C25F"));
                }


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       Intent intent = new Intent(TransactionsList.this, TransactionDetails.class);
                       intent.putExtra("TRANSACTION_NAME",model.getName());
                       intent.putExtra("TRANSACTION_CLASS",model.getClazz());
                       intent.putExtra("TRANSACTION_AMOUNT",model.getAmount());
                       intent.putExtra("TRANSACTION_TYPE",model.getType());
                       intent.putExtra("TRANSACTION_STATUS",model.getStatus());
                       intent.putExtra("TRANSACTION_DATE",model.getDate());
                       intent.putExtra("TRANSACTION_CHANNEL",model.getChannel());
                       intent.putExtra("TRANSACTION_REFERENCE",model.getreference());
                       startActivity(intent);
                    }
                });


            }

            @Override
            public TransactionsList.TransactionHolder onCreateViewHolder(ViewGroup group, int i) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.transaction, group, false);

                return new TransactionsList.TransactionHolder(view);
            }
        };

        transactionsRecycler.setLayoutManager(new LinearLayoutManager(this));
        transactionsRecycler.setAdapter(adapter);

    }


    public class TransactionHolder extends RecyclerView.ViewHolder {
        private TextView status, amount,name,date;
        private ImageView sign, clazz;

        public TransactionHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.transaction_status);
            name = itemView.findViewById(R.id.transactionName);
            date = itemView.findViewById(R.id.transaction_date);
            clazz = itemView.findViewById(R.id.transaction_class);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
