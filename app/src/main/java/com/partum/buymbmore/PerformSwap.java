package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

public class PerformSwap extends AppCompatActivity {


    private EditText airtimeAmount;
    private String network;
    private int ammount,percentage;
    private ImageView back;
    private TextView wePay,youGet;
    private String ussdCode;
    private Button done;
    private ImageView networkIcon;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_perform_swap);

        airtimeAmount = findViewById(R.id.airtime_amount);
        back = findViewById(R.id.back_to_airtime_to_cash_from_perform_swap);
        wePay = findViewById(R.id.we_pay);
        youGet = findViewById(R.id.you_get);
        done = findViewById(R.id.start_swap_data);
        networkIcon = findViewById(R.id.swap_network_image);

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        if (bundle != null) {
            network = bundle.getString("NETWORK_NAME");
            setTextViews();
            setNetworkIcon(network);
        }

        airtimeAmount.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {
                if (s.length() == 0){
                    youGet.setText("₦00");
                    setTextViews();
                }
                else{
                    switch (network) {
                        case "ETISALAT":
                            ussdCode = "*223*0000*"+s.toString()+"*08171101470#";
                            ammount = Integer.valueOf(s.toString());
                            percentage = (int) (0.76 * ammount);
                            youGet.setText("₦"+percentage);
                            break;
                        case "MTN":
                            ussdCode = "*600*07042989967*"+s.toString()+"*pin#";
                            ammount = Integer.valueOf(s.toString());
                            percentage = (int) (0.8 * ammount);
                            youGet.setText("₦"+percentage);
                            break;
                        case "GLO":
                            ussdCode = "*131*08154278237*"+s.toString()+"*0000#";
                            ammount = Integer.valueOf(s.toString());
                            percentage = (int) (0.8 * ammount);
                            youGet.setText("₦"+percentage);
                            break;
                        default:
                            ussdCode = "*432*1*08022490473*"+s.toString()+"*pin#";
                            ammount = Integer.valueOf(s.toString());
                            percentage = (int) (0.8 * ammount);
                            youGet.setText("₦"+percentage);
                    }

                }
            }
        });
    }

    private void setTextViews(){
        switch (network) {
            case "MTN":
                wePay.setText("Note: We pay 80% of the airtime price to your wallet");
                break;
            case "AIRTEL":
                wePay.setText("Note: We pay 80% of the airtime price to your wallet");

                break;
            case "GLO":
                wePay.setText("Note: We pay 80% of the airtime price to your wallet");

                break;
            case "ETISALAT":
                wePay.setText("Note: We pay 78% of the airtime price to your wallet");

                break;
            default:
                System.out.println("not reachable ");
        }

    done.setOnClickListener(v -> {
        Intent intent = new Intent(PerformSwap.this, FinalizeSwap.class);
        intent.putExtra("CODE",ussdCode);
        startActivity(intent);
    });

    }



    private void setNetworkIcon(String name){
        switch (name){
            case "MTN":
                networkIcon.setImageResource(R.drawable.mtn_logo);
                break;
            case "Airtel":
                networkIcon.setImageResource(R.drawable.airtel_logo);
                break;
            case "Glo":
                networkIcon.setImageResource(R.drawable.glo_logo);
            default:
                networkIcon.setImageResource(R.drawable.nine_mobile);

        }
    }
}
