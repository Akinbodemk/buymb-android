package com.partum.buymbmore.Startup;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Button;

import com.partum.buymbmore.Authentication.Login;
import com.partum.buymbmore.Authentication.SignUp;
import com.partum.buymbmore.R;
import com.partum.buymbmore.Utils.TypeWriter;

public class RootPage extends AppCompatActivity {


    private Button login,signup;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_root_page);

        login = findViewById(R.id.start_login);
        signup = findViewById(R.id.start_signup);

        login.setOnClickListener(v -> {
                startActivity(new Intent(RootPage.this, Login.class));
                //finish();
        });

        signup.setOnClickListener(v ->{
            startActivity(new Intent(RootPage.this, SignUp.class));
            //finish();
        });
    }


}
