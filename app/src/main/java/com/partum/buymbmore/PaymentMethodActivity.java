package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.EventModels.DataEventModel;
import com.partum.buymbmore.Models.Data;
import com.partum.buymbmore.Models.Transaction;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;

public class PaymentMethodActivity extends AppCompatActivity {

    private TextView networkName,bundleName,cardAmount,walletAmount;
    private CardView payWithCard,payWithWallet;
    private EditText phoneNumberEditText;
    private ImageView back;
    private Button ok;
    private RelativeLayout animationLayout,successLayout;
    private FirebaseFirestore db;
    private String email,bundlename,networkname, cardamount,walletamount,bundleID,phone;
    private FirebaseAuth mAuth;
    private String transactionreference;
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
    String date = simpleDateFormat.format(new Date());
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment_method);

        networkName = findViewById(R.id.network_name);
        bundleName = findViewById(R.id.bundle_name);
        cardAmount = findViewById(R.id.amount_to_pay_for_card);
        walletAmount = findViewById(R.id.amount_to_pay_for_wallet);
        payWithCard = findViewById(R.id.bank_card);
        payWithWallet = findViewById(R.id.wallet_card);
        back = findViewById(R.id.back_to_where);
        animationLayout = findViewById(R.id.payment_option_animation_layout);
        ok = findViewById(R.id.ok);
        successLayout = findViewById(R.id.success_layout);
        transactionreference = randomAlphaNumeric(12);

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        phoneNumberEditText = findViewById(R.id.phone_number);

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        if(bundle != null){
            bundlename = bundle.getString("BUNDLE_NAME");
            networkname = bundle.getString("NETWORK_NAME");
            cardamount = bundle.getString("BUNDLE_CARD_PRICE");
            walletamount = bundle.getString("BUNDLE_WALLET_PRICE");
            bundleID = bundle.getString("BUNDLE_ID");

            networkName.setText(networkname);
            bundleName.setText(bundlename);
            cardAmount.setText("Pay: "+cardamount);
            walletAmount.setText("Pay: "+ walletamount);

            payWithWallet.setOnClickListener( v -> {
                if(!TextUtils.isEmpty(phoneNumberEditText.getText())){
                    phone = phoneNumberEditText.getText().toString();
                    final EditText taskEditText = new EditText(this);
                    taskEditText.setPadding(30,0,0,20);
                    taskEditText.setSingleLine();
                    taskEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                    FrameLayout container = new FrameLayout(this);
                    FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                    params.leftMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                    params.rightMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                    taskEditText.setLayoutParams(params);
                    container.addView(taskEditText);
                    AlertDialog dialog = new AlertDialog.Builder(this)
                            .setTitle("Wallet Pin")
                            .setMessage("Please enter your wallet Pin")
                            .setView(container)
                            .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {

                                    if(!TextUtils.isEmpty(taskEditText.getText())){
                                        String pin = String.valueOf(taskEditText.getText());
                                        dialog.dismiss();
                                        animationLayout.setVisibility(View.VISIBLE);
                                        checkPinAndBalance(Integer.valueOf(walletamount), pin);

                                    }
                                    else{
                                        Toast.makeText(PaymentMethodActivity.this,"Please Enter a valid pin",Toast.LENGTH_SHORT).show();
                                    }
                                }
                            })
                            .setNegativeButton("Cancel", null)
                            .create();
                    dialog.show();
                }
                else{
                    Toast.makeText(PaymentMethodActivity.this, "Phone Number can not be empty",Toast.LENGTH_SHORT).show();
                }
            });

            payWithCard.setOnClickListener(v -> {
                if(!TextUtils.isEmpty(phoneNumberEditText.getText())){
                    phone = phoneNumberEditText.getText().toString();
                    Intent intent1 = new Intent(PaymentMethodActivity.this, SelectPaymentMode.class);
                    intent1.putExtra("MODE","Data");
                    intent1.putExtra("NETWORK_NAME",networkname);
                    intent1.putExtra("BUNDLE_NAME",bundlename);
                    intent1.putExtra("CARD_AMOUNT",cardamount);
                    intent1.putExtra("REFERENCE",transactionreference);
                    intent1.putExtra("PHONE_NUMBER",phone);
                    intent1.putExtra("BUNDLE_ID",Integer.valueOf(bundleID));
                    intent1.putExtra("NETWORK_ID",getNetworkID(networkname));
                    startActivity(intent1);
                }
                else{
                    Toast.makeText(PaymentMethodActivity.this, "Phone Number can not be empty",Toast.LENGTH_SHORT).show();
                }

            });


        }
    }

    public void checkPinAndBalance(int priceToPay, String pin){
        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                //String lastname = (String) document.get(getString(R.string.lastNameField));
                                long walletBalance = (long) document.get(getString(R.string.walletBalanceField));
                                String walletPin = (String) document.get(getString(R.string.walletPinField));
                                if(walletPin.equals(pin)){
                                    if(walletBalance >= priceToPay){
                                        Transaction transaction = new Transaction(networkname+" "+bundlename,"Data","Pending","debit",walletamount,
                                                date,transactionreference,"Wallet");
                                        transaction.uploadTransaction();
                                        Data data = new Data(phone,getNetworkID(networkname),Integer.valueOf(bundleID),transactionreference,Integer.valueOf(walletamount));
                                        data.BuyData();
                                    }
                                    else{
                                        animationLayout.setVisibility(View.GONE);
                                        Toast.makeText(PaymentMethodActivity.this,"Insufficient Funds",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    Toast.makeText(PaymentMethodActivity.this,"Wrong Pin",Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Log.d("HOME", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DataEventModel event){
        if(event.getState()){
            animationLayout.setVisibility(View.GONE);
            successLayout.setVisibility(View.VISIBLE);
            ok.setOnClickListener(v -> {
                startActivity(new Intent(PaymentMethodActivity.this,HomeActivity.class));
            });
        }else{
            Toast.makeText(PaymentMethodActivity.this,"Data Purchase Failed",Toast.LENGTH_SHORT).show();
            animationLayout.setVisibility(View.GONE);
        }
    }


    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    private int getNetworkID(String networkname){
        switch (networkname){
            case "MTN":
                return 1;
            case "Airtel":
                return 2;
            case "9 Mobile":
                return 3;
            default:
                return 4;
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
