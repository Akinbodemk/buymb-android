package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.os.CountDownTimer;
import android.util.Log;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.partum.buymbmore.Startup.RootPage;

public class SplashScreen extends AppCompatActivity {


    private CountDownTimer countDownTimer;
    private FirebaseAuth mAuth;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        mAuth = FirebaseAuth.getInstance();


        countDownTimer = new CountDownTimer(2000,1000) {
            @Override
            public void onTick(long l) {
                Log.d("CURRENT_TIME", "" + (l/1000));
            }

            @Override
            public void onFinish() {
                FirebaseUser currentUser = mAuth.getCurrentUser();
                if(currentUser != null){
                    startActivity(new Intent(SplashScreen.this, HomeActivity.class));
                    finish();
                }
                else{
                    startActivity(new Intent(SplashScreen.this, RootPage.class));
                    finish();
                }
            }
        }.start();
    }


}
