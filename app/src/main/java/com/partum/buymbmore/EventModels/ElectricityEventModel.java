package com.partum.buymbmore.EventModels;

public class ElectricityEventModel {
    String message;
    Boolean state;

    public ElectricityEventModel(String message, Boolean state) {
        this.message = message;
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getState() {
        return state;
    }
}
