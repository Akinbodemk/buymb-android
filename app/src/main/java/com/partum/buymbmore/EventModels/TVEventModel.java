package com.partum.buymbmore.EventModels;

public class TVEventModel {
    String message;
    Boolean state;

    public TVEventModel(String message, Boolean state) {
        this.message = message;
        this.state = state;
    }

    public String getMessage() {
        return message;
    }

    public Boolean getState() {
        return state;
    }
}
