package com.partum.buymbmore.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class SendMailClient {
    private static final String BaseUrl = "https://buy-mb-send-mail.herokuapp.com/";

    private static SendMailClient minstance;
    private Retrofit mretrofit;

    private SendMailClient(){
        mretrofit = new Retrofit.Builder().
                baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized SendMailClient getInstance(){
        if ( minstance == null){
            minstance = new SendMailClient();
        }
        return minstance;
    }

    public mailAPI getmailAPI(){
        return mretrofit.create(mailAPI.class);
    }
}
