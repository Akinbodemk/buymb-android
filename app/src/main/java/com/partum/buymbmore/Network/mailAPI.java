package com.partum.buymbmore.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface mailAPI {

    @FormUrlEncoded
    @POST("sendMail")
    Call<ResponseBody> sendMail(
            @Header("token") String token,
            @Field("provider") String provider,
            @Field("message") String message,
            @Field("email") String email

    );
}
