package com.partum.buymbmore.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class TVClient {

        private static final String BaseUrl = "https://sandbox.vtpass.com/api/";

        private static TVClient minstance;
        private Retrofit mretrofit;

        private TVClient(){
            mretrofit = new Retrofit.Builder().
                    baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        public static synchronized TVClient getInstance(){
            if ( minstance == null){
                minstance = new TVClient();
            }
            return minstance;
        }

        public TVAPI getTVAPI(){
            return mretrofit.create(TVAPI.class);
        }

}
