package com.partum.buymbmore.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PowerClient {

    private static final String BaseUrl = " https://vtpass.com/api/";

    private static PowerClient minstance;
    private Retrofit mretrofit;

    private PowerClient(){
        mretrofit = new Retrofit.Builder().
                baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static synchronized PowerClient getInstance(){
        if ( minstance == null){
            minstance = new PowerClient();
        }
        return minstance;
    }

    public PowerAPI getPowerAPI(){
        return mretrofit.create(PowerAPI.class);
    }
}
