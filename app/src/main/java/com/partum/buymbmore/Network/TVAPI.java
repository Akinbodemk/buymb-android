package com.partum.buymbmore.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TVAPI {

    @GET("service-variations?")
    Call<ResponseBody> getVariations(
            @Header("Authorization") String auth_header,
            @Query("serviceID") String serviceID
    );


    @FormUrlEncoded
    @POST("merchant-verify")
    Call<ResponseBody> verifyCode(
            @Header("Authorization") String token,
            @Field("billersCode") String billersCode,
            @Field("serviceID") String serviceID
    );

    @FormUrlEncoded
    @POST("pay")
    Call<ResponseBody> payForTV(
            @Header("Authorization") String token,
            @Field("request_id") String requestID,
            @Field("serviceID") String serviceID,
            @Field("billersCode") String billersCode ,
            @Field("variation_code") String varCode,
            @Field("phone") String phone

    );
}
