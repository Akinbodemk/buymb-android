package com.partum.buymbmore.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.Path;

public interface VerifyAPI {

    @GET("verify/{reference}")
    Call<ResponseBody> verifyTransaction(
            @Header("Authorization") String token,
            @Path("reference") String transactionReference

    );
}
