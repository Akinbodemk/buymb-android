package com.partum.buymbmore.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class VerifyClient {
    private static final String BaseUrl = "https://api.paystack.co/transaction/";

    private static VerifyClient minstance;
    private Retrofit mretrofit;

    private VerifyClient(){
        mretrofit = new Retrofit.Builder().
                baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
                .build();
    }

    public static  synchronized VerifyClient getInstance(){
        if ( minstance == null){
            minstance = new VerifyClient();
        }
        return minstance;
    }

    public VerifyAPI getTripsAPI(){
        return mretrofit.create(VerifyAPI.class);
    }
}
