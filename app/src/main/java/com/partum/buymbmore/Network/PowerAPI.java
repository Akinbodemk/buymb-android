package com.partum.buymbmore.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface PowerAPI {


    @FormUrlEncoded
    @POST("merchant-verify")
    Call<ResponseBody> verifyCode(
            @Header("Authorization") String token,
            @Field("billersCode") String billersCode,
            @Field("serviceID") String serviceID,
            @Field("type") String type
    );

    @FormUrlEncoded
    @POST("pay")
    Call<ResponseBody> payForPower(
            @Header("Authorization") String token,
            @Field("request_id") String requestID,
            @Field("serviceID") String serviceID,
            @Field("billersCode") String billersCode ,
            @Field("variation_code") String varCode,
            @Field("amount") int amount,
            @Field("phone") String phone

    );
}
