package com.partum.buymbmore.Network;

import com.google.gson.JsonObject;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Header;
import retrofit2.http.Headers;
import retrofit2.http.POST;

public interface DataAPI {

    @Headers( "Content-Type: application/json" )
    @POST("data/purchase")
    Call<ResponseBody> purchaseData(
            @Header("Authorization") String token,
            @Body JsonObject body
    );
}
