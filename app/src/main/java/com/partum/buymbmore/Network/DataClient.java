package com.partum.buymbmore.Network;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class DataClient {

        private static final String BaseUrl = "https://smeplug.ng/api/v1/";

        private static DataClient minstance;
        private Retrofit mretrofit;

        private DataClient(){
            mretrofit = new Retrofit.Builder().
                    baseUrl(BaseUrl).addConverterFactory(GsonConverterFactory.create())
                    .build();
        }

        public static  synchronized DataClient getInstance(){
            if ( minstance == null){
                minstance = new DataClient();
            }
            return minstance;
        }

        public DataAPI getDataAPI(){
            return mretrofit.create(DataAPI.class);
        }

}
