package com.partum.buymbmore.Network;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.Header;
import retrofit2.http.POST;

public interface SignUpAPI {

    @FormUrlEncoded
    @POST("merchant-verify")
    Call<ResponseBody> sendEmail(
            @Field("email") String email
    );
}
