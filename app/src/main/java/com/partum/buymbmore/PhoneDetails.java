package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

public class PhoneDetails extends AppCompatActivity {

    private TextView phoneDescription,phonePrice,longDescription;
    private ImageView phoneImage;
    private Button proceed;
    private ImageView back;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_details);

        back = findViewById(R.id.back_from_phone_details);
        proceed = findViewById(R.id.proceed_to_order);
        phoneImage = findViewById(R.id.phone_image);
        phoneDescription = findViewById(R.id.short_description);
        longDescription = findViewById(R.id.long_description);
        phonePrice = findViewById(R.id.phone_price);


        Intent intent = getIntent();
        Bundle bundle = intent.getExtras();

        if(bundle != null){
            int price = bundle.getInt("PRICE");
            String urlz = bundle.getString("IMAGE_URL");
            String shortDesc = bundle.getString("SHORT_DESCRIPTION");
            phoneDescription.setText(shortDesc);
            longDescription.setText(bundle.getString("LONG_DESCRIPTION"));
            phonePrice.setText("₦"+price);
            Log.d("URL","this is url: "+ urlz);
            showPhone(bundle.getString("IMAGE_URL"));
        }

        proceed.setOnClickListener( v -> {
          composeEmail("services@Buymb.com","ORDER "+ bundle.getString("SHORT_DESCRIPTION"),"Hello i would like to purchase the "+ bundle.getString("SHORT_DESCRIPTION")+ " thank you.");
        });
    }


    public void showPhone(String url){
      Picasso.get().load(url).into(phoneImage);
    }

    public void composeEmail(String address, String subject,String message) {
        Intent intent = new Intent(Intent.ACTION_SENDTO);
        intent.setData(Uri.parse("mailto:support@hubryde.com")); // only email apps should handle this
        intent.putExtra(Intent.EXTRA_SUBJECT, subject);
        intent.putExtra(Intent.EXTRA_TEXT,message);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
