package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class TransactionDetails extends AppCompatActivity {


    private ImageView back,clazzIcon, typeIcon;
    private TextView name,status,amount,date,channel,reference,type;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_transaction_details);
        back = findViewById(R.id.back_from_details);
        clazzIcon = findViewById(R.id.detail_class_icon);
        typeIcon = findViewById(R.id.details_sign);
        name = findViewById(R.id.details_name_text);
        status = findViewById(R.id.details_status);
        amount = findViewById(R.id.details_amount);
        date = findViewById(R.id.details_date);
        channel = findViewById(R.id.details_channel);
        reference = findViewById(R.id.details_reference);
        type = findViewById(R.id.details_type);


        back.setOnClickListener( v -> {
            onBackPressed();
        });

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        if(bundle != null){
            reference.setText(bundle.getString("TRANSACTION_REFERENCE"));
            type.setText(bundle.getString("TRANSACTION_TYPE"));
            channel.setText(bundle.getString("TRANSACTION_CHANNEL"));
            date.setText(bundle.getString("TRANSACTION_DATE"));
            name.setText(bundle.getString("TRANSACTION_NAME"));
            amount.setText("₦"+bundle.getString("TRANSACTION_AMOUNT"));

            String stats  = bundle.getString("TRANSACTION_STATUS");

            if (stats.equals("Pending")){
                status.setText("Pending");
                status.setTextColor(Color.parseColor( "#E76E51"));
            }
            else if (stats.equals("Failed")){
                status.setText("Failed");
                status.setTextColor(Color.RED);
            }
            else {
                status.setText("Successful");
                status.setTextColor(Color.parseColor("#68C25F"));
            }

            String clazz = bundle.getString("TRANSACTION_CLASS");
            switch(clazz) {
                case "TV":
                    clazzIcon.setImageResource(R.drawable.ic_live_tv);
                case "Power":
                    clazzIcon.setImageResource(R.drawable.ic_lightbulb);
                case "Funding":
                    clazzIcon.setImageResource(R.drawable.ic_add_to_queue);
                default:
                    clazzIcon.setImageResource(R.drawable.ic_phone);
            }

            String typ = bundle.getString("TRANSACTION_TYPE");
            if (typ.equals("credit")) {
               typeIcon.setImageResource(R.drawable.ic_add);
            }
            else {
                typeIcon.setImageResource(R.drawable.ic_remove);
            }
        }

        reference.setOnClickListener(v -> {
            setClipboard(this,reference.getText().toString());
        });
    }


    private void setClipboard(Context context, String text) {
        if(android.os.Build.VERSION.SDK_INT < android.os.Build.VERSION_CODES.HONEYCOMB) {
            ClipboardManager clipboard = (ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            ClipData clip = ClipData.newPlainText("Copied", text);
            clipboard.setPrimaryClip(clip);
            Toast.makeText(context,"Copied!!",Toast.LENGTH_SHORT).show();
        } else {
            android.content.ClipboardManager clipboard = (android.content.ClipboardManager) context.getSystemService(Context.CLIPBOARD_SERVICE);
            android.content.ClipData clip = android.content.ClipData.newPlainText("Copied Text", text);
            clipboard.setPrimaryClip(clip);
        }
    }
}
