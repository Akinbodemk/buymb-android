package com.partum.buymbmore;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.partum.buymbmore.EventModels.DataEventModel;
import com.partum.buymbmore.Models.Verifier;
import com.wang.avi.AVLoadingIndicatorView;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.Date;

import co.paystack.android.Paystack;
import co.paystack.android.PaystackSdk;
import co.paystack.android.Transaction;
import co.paystack.android.model.Card;
import co.paystack.android.model.Charge;

public class PaymentActivity extends AppCompatActivity {

    private ImageView back;
    private Button proceed;
    private AVLoadingIndicatorView payProgress;
    private String reference,amount,cardNumber,cvv;
    private int month, year;
    private FirebaseAuth mAuth;
    private String mode;
    private String email;
    private String networkName, bundleName, phoneNumber;
    private int networkID, bundleID;
    private com.partum.buymbmore.Models.Transaction cardTransaction;
    private EditText amountEditText,cardNumberEditText,monthEditText,yearEditText,cvvEditText;
    private RelativeLayout successLayout;
    private Button ok;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_payment);

        PaystackSdk.initialize(getApplicationContext());

        back = findViewById(R.id.back_from_payment);
        proceed = findViewById(R.id.proceed);
        payProgress = findViewById(R.id.pay_progress);
        amountEditText = findViewById(R.id.amount);
        cardNumberEditText = findViewById(R.id.card_number);
        monthEditText = findViewById(R.id.month);
        yearEditText = findViewById(R.id.year);
        cvvEditText = findViewById(R.id.cvv);
        successLayout = findViewById(R.id.success_layout_for_card_payment);
        ok = findViewById(R.id.ok_for_card);


        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        Intent intent = getIntent();

        Bundle bundle = intent.getExtras();

        if(bundle != null){
            mode = bundle.getString("MODE");
            switch (mode){
                case "Data":
                    networkName = bundle.getString("NETWORK_NAME");
                    bundleName = bundle.getString("BUNDLE_NAME");
                    phoneNumber = bundle.getString("PHONE_NUMBER");
                    networkID = bundle.getInt("NETWORK_ID");
                    bundleID = bundle.getInt("BUNDLE_ID");
                    amount = bundle.getString("CARD_AMOUNT");
                    amountEditText.setText(amount);
                    break;
                case "Funding":
                    break;
                 default:
                     System.out.println("UNREACHABLE");
            }
        }

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        proceed.setOnClickListener(v -> {
            if(TextUtils.isEmpty(amountEditText.getText()) || TextUtils.isEmpty(cardNumberEditText.getText())
            || TextUtils.isEmpty(monthEditText.getText()) || TextUtils.isEmpty(yearEditText.getText()) || TextUtils.isEmpty(cvvEditText.getText())){
                Toast.makeText(PaymentActivity.this, "Please Fill in all the details",Toast.LENGTH_SHORT).show();
                proceed.setVisibility(View.VISIBLE);
                payProgress.setVisibility(View.GONE);
            }
            else{
                amount = amountEditText.getText().toString();
                cardNumber = cardNumberEditText.getText().toString();
                cvv = cvvEditText.getText().toString();
                month = Integer.valueOf(monthEditText.getText().toString());
                year = Integer.valueOf(yearEditText.getText().toString());
                payProgress.setVisibility(View.VISIBLE);
                proceed.setVisibility(View.GONE);
                startCharge(cardNumber,cvv,month,year);
            }

        });

    }

    private void startCharge(String cardNumber, String cardCVV, int expiryMonth, int expiryYear){

        Log.d("DETAILS","number: "+ cardNumber+ " month: "+expiryMonth+ " cvv: "+cardCVV+ " year: "+ year);
        Card card = new Card(cardNumber, expiryMonth, expiryYear, cardCVV);
        if(card.isValid()){
            performCharge(card);
        }else{
            proceed.setVisibility(View.VISIBLE);
            payProgress.setVisibility(View.GONE);
            Toast.makeText(PaymentActivity.this,"Invalid Card Details", Toast.LENGTH_SHORT).show();
        }
    }

    private  void performCharge(Card card){
        Charge charge = new Charge();
        charge.setAmount(Integer.valueOf(amount));
        charge.setEmail(email);
        charge.setCard(card); //sets the card to charge
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
        String date = simpleDateFormat.format(new Date());



        PaystackSdk.chargeCard(PaymentActivity.this, charge, new Paystack.TransactionCallback() {
            @Override
            public void onSuccess(Transaction transaction) {
                switch (mode){
                    case "Data":
                        reference = transaction.getReference();
                        cardTransaction = new com.partum.buymbmore.Models.Transaction(networkName+" "+bundleName,"Data","Pending","debit",amount,
                                date,reference,"Card");
                        cardTransaction.uploadTransaction();
                        Verifier verifier = new Verifier(reference,email,"Data",amount,networkID,bundleID,phoneNumber);
                        verifier.verifyTransaction();
                        break;
                    case "Funding":
                        reference = transaction.getReference();
                        cardTransaction = new com.partum.buymbmore.Models.Transaction("Wallet Funding","Funding","Pending",
                                "credit",amount,date,reference,"Card");
                        cardTransaction.uploadTransaction();
                        //verifyTransaction
                        Verifier verifier1 = new Verifier(reference,email,"Funding",amount);
                        verifier1.verifyTransaction();
                        onBackPressed();
                        break;
                     default:
                         Log.d("STATE","UNREACHABLE");
                }

            }

            @Override
            public void beforeValidate(Transaction transaction) {
                reference = transaction.getReference();
                // This is called only before requesting OTP.
                // Save reference so you may send to server. If
                // error occurs with OTP, you should still verify on server.
            }

            @Override
            public void onError(Throwable error, Transaction transaction) {
                proceed.setVisibility(View.VISIBLE);
                payProgress.setVisibility(View.GONE);
                Toast.makeText(PaymentActivity.this,"Transaction Failed "+ error.getLocalizedMessage(), Toast.LENGTH_SHORT).show();
            }

        });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(DataEventModel event){
        if(event.getState()){
               successLayout.setVisibility(View.VISIBLE);
               ok.setOnClickListener( v -> {
                   startActivity(new Intent(PaymentActivity.this, HomeActivity.class));
               });
        }else{
            Toast.makeText(PaymentActivity.this,"Data Purchase Failed",Toast.LENGTH_SHORT).show();
            proceed.setVisibility(View.VISIBLE);
            payProgress.setVisibility(View.INVISIBLE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }
}
