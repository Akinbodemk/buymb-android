package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.firebase.ui.firestore.FirestoreRecyclerAdapter;
import com.firebase.ui.firestore.FirestoreRecyclerOptions;
import com.google.firebase.firestore.EventListener;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.FirebaseFirestoreException;
import com.google.firebase.firestore.Query;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Models.Phone;

public class PhoneList extends AppCompatActivity {

    private ImageView back;

    private TextView phoneType;

    private RecyclerView phones;
    private String phone_type;
    private FirebaseFirestore db;
    private FirestoreRecyclerAdapter adapter;
    private LinearLayout no_phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_phone_list);

        back = findViewById(R.id.back_from_phone_list);
        phones = findViewById(R.id.phones);
        no_phone = findViewById(R.id.no_phone);

        back.setOnClickListener(v -> {
             onBackPressed();
        });


        phoneType = findViewById(R.id.phone_tyoe);

        db = FirebaseFirestore.getInstance();



        Intent intent = getIntent();

       Bundle bundle = intent.getExtras();

       if (bundle != null){
           int type = bundle.getInt("PHONE_TYPE");
           if (type == 0){
               phoneType.setText("Buy Iphone");
               phone_type = "iphone";
           }
           else {
               phoneType.setText("Buy Samsung");
               phone_type = "samsung";
           }
       }


        Query phonez  = db.collection("Phones").whereEqualTo("type", phone_type);
        phonez.addSnapshotListener(new EventListener<QuerySnapshot>() {
            @Override
            public void onEvent(@Nullable QuerySnapshot queryDocumentSnapshots, @Nullable FirebaseFirestoreException e) {
                if(!(queryDocumentSnapshots == null)){
                    no_phone.setVisibility(View.GONE);
                    phones.setVisibility(View.VISIBLE);
                }
            }
        });

        FirestoreRecyclerOptions<Phone> options = new FirestoreRecyclerOptions.Builder<Phone>()
                .setQuery(phonez, Phone.class)
                .build();

        adapter = new FirestoreRecyclerAdapter<Phone, PhoneList.PhoneHolder>(options) {
            @Override
            public void onBindViewHolder(PhoneList.PhoneHolder holder, int position, Phone model) {
               holder.phonee_name.setText(model.getName());


                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                       Intent intent1 = new Intent(PhoneList.this,PhoneDetails.class);
                        Log.d("URL", "this is model url: "+ model.getImageLink());
                       intent1.putExtra("IMAGE_URL",model.getImageLink());
                       intent1.putExtra("PRICE",model.getPrice());
                       intent1.putExtra("LONG_DESCRIPTION",model.getFullDescription());
                       intent1.putExtra("SHORT_DESCRIPTION",model.getShortDescription());
                       intent1.putExtra("PRODUCT_NAME",model.getName());
                       startActivity(intent1);

                    }
                });


            }

            @Override
            public PhoneList.PhoneHolder onCreateViewHolder(ViewGroup group, int i) {
                // Create a new instance of the ViewHolder, in this case we are using a custom
                // layout called R.layout.message for each item
                View view = LayoutInflater.from(group.getContext())
                        .inflate(R.layout.item, group, false);

                return new PhoneList.PhoneHolder(view);
            }
        };

        phones.setLayoutManager(new LinearLayoutManager(this));
        phones.setAdapter(adapter);





    }


    public class PhoneHolder extends RecyclerView.ViewHolder {
        private TextView phonee_name;

        public PhoneHolder(@NonNull View itemView) {
            super(itemView);
           phonee_name = itemView.findViewById(R.id.item_name);

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        adapter.startListening();
    }

    @Override
    protected void onStop() {
        super.onStop();
        adapter.stopListening();
    }
}
