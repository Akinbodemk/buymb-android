package com.partum.buymbmore.Adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.recyclerview.widget.RecyclerView;

import com.partum.buymbmore.R;

public class HomeAdapter extends RecyclerView.Adapter<HomeAdapter.HomeItemViewHolder> {


    private View.OnClickListener mClickListener;

    private String[] titles = {
            "Buy Airtime and Data", "Pay Bills", "Airtime to Cash", "Trade gift cards and BTC", "Airtime Swap",
            "Buy Phone", "Other Services"
    };



    private int[] images = {
            R.drawable.ic_phone_icon,
            R.drawable.ic_pay_bills,
            R.drawable.ic_airtime_to_cash,
            R.drawable.ic_bitcoine,
            R.drawable.ic_airtime_swap,
            R.drawable.ic_buy_phone,
            R.drawable.ic_other_services
    };


    public HomeAdapter() {

    }

    @NonNull
    @Override
    public HomeItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.sections, viewGroup, false);
        RecyclerView.ViewHolder holder = new HomeItemViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return (HomeItemViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull HomeItemViewHolder homeItemViewHolder, int i) {

        final View itemView = homeItemViewHolder.itemView;

          homeItemViewHolder.title.setText(titles[i]);
          homeItemViewHolder.imageView.setImageResource(images[i]);


    }

    @Override
    public int getItemCount() {
        return titles.length;
    }


    public class HomeItemViewHolder extends RecyclerView.ViewHolder {
        private TextView  title;
        private ImageView imageView;


        public HomeItemViewHolder(@NonNull View itemView) {
            super(itemView);

            title = itemView.findViewById(R.id.section_name);
            imageView = itemView.findViewById(R.id.section_icon);



        }
    }


    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}

