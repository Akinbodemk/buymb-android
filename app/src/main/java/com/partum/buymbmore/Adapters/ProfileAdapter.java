package com.partum.buymbmore.Adapters;

import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.partum.buymbmore.R;

public class ProfileAdapter extends RecyclerView.Adapter<ProfileAdapter.ProfileItemViewHolder> {


    private View.OnClickListener mClickListener;

    private String[] titles = {
            "Your Account", "Withdraw Funds", "Enquiries and Support", "Contact", "Logout"
    };

    private  int[] images = {
            R.drawable.ic_user,
            R.drawable.ic_withdraw,
            R.drawable.ic_support,
            R.drawable.ic_contact,
            R.drawable.ic_logout

    };



    public ProfileAdapter() {

    }

    @NonNull
    @Override
    public ProfileItemViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.more_items, viewGroup, false);
        RecyclerView.ViewHolder holder = new ProfileItemViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return (ProfileItemViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull ProfileItemViewHolder profileItemViewHolder, int i) {

        if( i == 4){
            profileItemViewHolder.title.setText(titles[i]);
            profileItemViewHolder.title.setTextColor(Color.parseColor("#FF0B0B"));
            profileItemViewHolder.chevron.setImageResource(R.drawable.chevron_red);
            profileItemViewHolder.icon.setImageResource(images[i]);
        }
        else{
            profileItemViewHolder.title.setText(titles[i]);
            profileItemViewHolder.icon.setImageResource(images[i]);
        }




    }

    @Override
    public int getItemCount() {
        return titles.length;
    }


    public class ProfileItemViewHolder extends RecyclerView.ViewHolder {
        private TextView title;
        private ImageView icon,chevron;


        public ProfileItemViewHolder(@NonNull View itemView) {
            super(itemView);
            title = itemView.findViewById(R.id.more_item_name);
            icon = itemView.findViewById(R.id.more_item_image);
            chevron = itemView.findViewById(R.id.chevron);

        }
    }


    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }

}

