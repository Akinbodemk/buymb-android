package com.partum.buymbmore.Adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.partum.buymbmore.Models.Transaction;
import com.partum.buymbmore.R;

import java.util.ArrayList;

public class WalletTransactionAdapter extends RecyclerView.Adapter<WalletTransactionAdapter.WalletTransactionViewHolder> {


    private ArrayList<Transaction> datalist;
    private View.OnClickListener mClickListener;


    public WalletTransactionAdapter(ArrayList<Transaction> datalist) {
        this.datalist = datalist;
    }

    @NonNull
    @Override
    public WalletTransactionViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.transaction, viewGroup, false);
        RecyclerView.ViewHolder holder = new WalletTransactionViewHolder(view);
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mClickListener.onClick(view);
            }
        });
        return (WalletTransactionViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull WalletTransactionViewHolder walletTransactionViewHolder, int i) {

    }

    @Override
    public int getItemCount() {
        return (datalist != null) ? datalist.size() : 0;
    }

    public class WalletTransactionViewHolder extends RecyclerView.ViewHolder {
        private TextView status, amount,name,date;
        private ImageView sign, clazz;

        public WalletTransactionViewHolder(@NonNull View itemView) {
            super(itemView);
            status = itemView.findViewById(R.id.transaction_status);
            name = itemView.findViewById(R.id.transactionName);
            date = itemView.findViewById(R.id.transaction_date);
            clazz = itemView.findViewById(R.id.transaction_class);


        }
    }


    public void setClickListener(View.OnClickListener callback) {
        mClickListener = callback;
    }


    public void clear() {
        datalist.clear();
        notifyDataSetChanged();
    }
}