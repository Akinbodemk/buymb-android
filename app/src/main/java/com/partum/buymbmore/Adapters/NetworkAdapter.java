package com.partum.buymbmore.Adapters;

import android.content.Context;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.partum.buymbmore.R;

import java.util.ArrayList;

public class NetworkAdapter extends RecyclerView.Adapter<NetworkAdapter.NetworkViewHolder> {


    private ArrayList<String> datalist;
    private Context context;

    //declare interface
    private OnItemClicked onClick;

    //make interface like this
    public interface OnItemClicked {
        void onItemClick(int position);
    }


    public NetworkAdapter(Context context,ArrayList<String> datalist) {
        this.datalist = datalist;
        this.context = context;
    }

    @NonNull
    @Override
    public NetworkViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        LayoutInflater layoutInflater = LayoutInflater.from(viewGroup.getContext());
        View view = layoutInflater.inflate(R.layout.bundle, viewGroup, false);
        RecyclerView.ViewHolder holder = new NetworkViewHolder(view);

        return (NetworkViewHolder) holder;
    }

    @Override
    public void onBindViewHolder(@NonNull NetworkViewHolder networkViewHolder, int i) {
        networkViewHolder.netowrk.setText(datalist.get(i));
        if((i % 2) == 0){
            networkViewHolder.bakcground.setBackgroundColor(Color.parseColor("#eaeaea"));
        } else {
            networkViewHolder.bakcground.setBackgroundColor(Color.parseColor("#ffffff"));
        }
        networkViewHolder.bakcground.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onClick.onItemClick(i);
            }
        });
    }



    @Override
    public int getItemCount() {
        return (datalist != null) ? datalist.size() : 0;
    }



    public class NetworkViewHolder extends RecyclerView.ViewHolder {
        private TextView netowrk;
        private RelativeLayout bakcground;

        public NetworkViewHolder(@NonNull View itemView) {
            super(itemView);
            netowrk = itemView.findViewById(R.id.network_bundle);
            bakcground = itemView.findViewById(R.id.bundle_background);
        }
    }







    public void clear() {
        datalist.clear();
    }

    public void addAll(ArrayList<String> newData){
        datalist.addAll(newData);
        notifyDataSetChanged();
    }

    public void setOnClick(OnItemClicked onClick)
    {
        this.onClick=onClick;
    }
}
