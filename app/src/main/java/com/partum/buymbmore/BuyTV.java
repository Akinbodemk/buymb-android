package com.partum.buymbmore;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.InputType;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.firestore.FirebaseFirestore;
import com.google.firebase.firestore.QueryDocumentSnapshot;
import com.google.firebase.firestore.QuerySnapshot;
import com.partum.buymbmore.Adapters.NetworkAdapter;
import com.partum.buymbmore.EventModels.TVEventModel;
import com.partum.buymbmore.Models.Bouqet;
import com.partum.buymbmore.Models.TV;
import com.partum.buymbmore.Models.Transaction;
import com.partum.buymbmore.Network.TVClient;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class BuyTV extends AppCompatActivity {

    private ImageView back;
    private ArrayList<String>  billers = new ArrayList<String>();
    private ArrayList<String>  bouqetNames = new ArrayList<>();
    private ArrayList<Bouqet> bouqets = new ArrayList<>();
    private String billerName;
    private RelativeLayout successLayout,animationLayout;
    private EditText userDecoderNumber,amountEdittext,phoneEdittext;
    private Spinner billerSpinner,bouqetSpinner;
    private Button pay,ok;
    private FirebaseFirestore db;
    private String email;
    private FirebaseAuth mAuth;
    private String transactionreference;
    private TextView cutosmerName;
    private TV tv;

    private String credentials =  "tns.ccare@gmail.com"+ ":" + "08169303901";
    // create Base64 encoded string
    private final String basicAuth =
            "Basic " + Base64.encodeToString(credentials.getBytes(), Base64.NO_WRAP);

    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("dd MMMM yyyy HH:mm:ss");
    String date = simpleDateFormat.format(new Date());
    private static final String ALPHA_NUMERIC_STRING = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";

    private String serviceID,bouqetname,amount,variationCode,billersCode,phone;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_buy_tv);

        back = findViewById(R.id.back_from_buy_tv);
        billerSpinner = findViewById(R.id.tv_biller_spinner);
        bouqetSpinner = findViewById(R.id.tv_bouqet_spinner);
        userDecoderNumber = findViewById(R.id.tv_reference);
        successLayout = findViewById(R.id.tv_success_layout);
        animationLayout = findViewById(R.id.tv_animation_layout);
        amountEdittext = findViewById(R.id.tv_amount);
        pay = findViewById(R.id.start_pay_tv_bills);
        phoneEdittext = findViewById(R.id.tv_phone);
        ok = findViewById(R.id.ok_for_tv);
        cutosmerName = findViewById(R.id.customer_name);

        transactionreference = randomAlphaNumeric(12);

        back.setOnClickListener(v -> {
            onBackPressed();
        });

        db = FirebaseFirestore.getInstance();
        mAuth = FirebaseAuth.getInstance();
        FirebaseUser user = mAuth.getCurrentUser();
        email = user.getEmail();

        billers.add("DSTV");
        billers.add("GOTV");
        billers.add("Startimes");

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BuyTV.this, android.R.layout.simple_spinner_item, billers);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        billerSpinner.setAdapter(adapter);


        billerSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!billers.isEmpty()){
                    bouqetNames.clear();
                    bouqets.clear();
                    getBouqets(getServiceID(billers.get(position)));
                    billerName = billers.get(position);
                    animationLayout.setVisibility(View.VISIBLE);
                }

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });


        bouqetSpinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                if(!bouqetNames.isEmpty()){
                    Bouqet currentBouqet = bouqets.get(position);

                    serviceID = currentBouqet.getServiceID();
                    bouqetname = currentBouqet.getName();
                    variationCode = currentBouqet.getVariationCode();
                    amountEdittext.setText(currentBouqet.getAmount());
                    amount = currentBouqet.getAmount();

                    //startActivity(intent);
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        pay.setOnClickListener(v -> {
            if(!TextUtils.isEmpty(phoneEdittext.getText()) || !TextUtils.isEmpty(userDecoderNumber.getText())){
                phone = phoneEdittext.getText().toString();
                billersCode = userDecoderNumber.getText().toString();
                final EditText taskEditText = new EditText(this);
                taskEditText.setPadding(30,0,0,20);
                taskEditText.setSingleLine();
                taskEditText.setInputType(InputType.TYPE_NUMBER_VARIATION_PASSWORD);
                FrameLayout container = new FrameLayout(this);
                FrameLayout.LayoutParams params = new  FrameLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
                params.leftMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                params.rightMargin = getResources().getDimensionPixelSize(R.dimen.edt_margin);
                taskEditText.setLayoutParams(params);
                container.addView(taskEditText);
                AlertDialog dialog = new AlertDialog.Builder(this)
                        .setTitle("Wallet Pin")
                        .setMessage("Please enter your wallet Pin")
                        .setView(container)
                        .setPositiveButton("Done", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {

                                if(!TextUtils.isEmpty(taskEditText.getText())){
                                    String pin = String.valueOf(taskEditText.getText());
                                    dialog.dismiss();
                                    double d = Double.parseDouble(amount);
                                    int i = (int) d;
                                    tv = new TV(transactionreference,billersCode,serviceID,variationCode,phone,i);
                                    animationLayout.setVisibility(View.VISIBLE);
                                    checkPinAndBalance(i, pin);

                                }
                                else{
                                    Toast.makeText(BuyTV.this,"Please Enter a valid pin",Toast.LENGTH_SHORT).show();
                                }
                            }
                        })
                        .setNegativeButton("Cancel", null)
                        .create();
                dialog.show();
            }
            else{
                Toast.makeText(BuyTV.this, "Phone Number can not be empty",Toast.LENGTH_SHORT).show();
            }
        });




    }


    private String getServiceID(String billerName){
      switch (billerName){
          case "DSTV":
              return "dstv";
          case "GOTV":
              return "gotv";
          default:
              return "startimes";
      }
    }


    public void getBouqets(String service_id) {
        Call<ResponseBody> call = TVClient.getInstance().getTVAPI().getVariations(basicAuth,service_id);
        call.enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                if(response.code() == 200){
                    try {
                        JSONObject jsonObject = new JSONObject(response.body().string());
                        JSONObject content = jsonObject.getJSONObject("content");
                        JSONArray variations = content.getJSONArray("varations");
                        for( int i = 0; i < variations.length(); i++){
                            JSONObject variation = variations.getJSONObject(i);
                            String name = variation.getString("name");
                            String amount = variation.getString("variation_amount");
                            Log.d("TVPAYMMENT", "anount: "+ amount + " name: "+ name);
                            String code = variation.getString("variation_code");
                            Bouqet bouqet = new Bouqet(service_id,name,code,amount);
                            bouqetNames.add(name);
                            bouqets.add(bouqet);
                        }
                        animationLayout.setVisibility(View.GONE);
                        ArrayAdapter<String> adapter = new ArrayAdapter<String>(BuyTV.this, android.R.layout.simple_spinner_item, bouqetNames);
                        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
                        bouqetSpinner.setAdapter(adapter);



                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
                else{
                    try {

                        JSONObject errorObject = new JSONObject(response.errorBody().string());
                        Log.d("TV",errorObject.toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }

                }
            }

            @Override
            public void onFailure(Call<ResponseBody> call, Throwable t) {
                Log.d("TV",t.getMessage());
            }
        });


    }


    public static String randomAlphaNumeric(int count) {
        StringBuilder builder = new StringBuilder();
        while (count-- != 0) {
            int character = (int)(Math.random()*ALPHA_NUMERIC_STRING.length());
            builder.append(ALPHA_NUMERIC_STRING.charAt(character));
        }
        return builder.toString();
    }

    public void checkPinAndBalance(int priceToPay, String pin){
        db.collection(getString(R.string.users))
                .whereEqualTo(getString(R.string.emailField),email )
                .get()
                .addOnCompleteListener(new OnCompleteListener<QuerySnapshot>() {
                    @Override
                    public void onComplete(@NonNull Task<QuerySnapshot> task) {
                        if (task.isSuccessful()) {
                            for (QueryDocumentSnapshot document : task.getResult()) {
                                Log.d("HOME", document.getId() + " => " + document.getData());
                                //String lastname = (String) document.get(getString(R.string.lastNameField));
                                long walletBalance = (long) document.get(getString(R.string.walletBalanceField));
                                String walletPin = (String) document.get(getString(R.string.walletPinField));
                                if(walletPin.equals(pin)){
                                    if(walletBalance >= priceToPay){
                                        Transaction transaction = new Transaction(bouqetname,"TV","Pending","debit",amount,
                                                date,transactionreference,"Wallet");
                                        transaction.uploadTransaction();
                                        tv.verifyBillerCode();

                                    }else{
                                        animationLayout.setVisibility(View.GONE);
                                        Toast.makeText(BuyTV.this,"Insufficient Funds",Toast.LENGTH_SHORT).show();
                                    }
                                }
                                else{
                                    Toast.makeText(BuyTV.this,"Wrong Pin",Toast.LENGTH_SHORT).show();
                                }

                            }
                        } else {
                            Log.d("HOME", "Error getting documents: ", task.getException());
                        }
                    }
                });
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onMessageEvent(TVEventModel event){
        if(event.getState()){
            if(event.getMessage().equals("Successful")){
                animationLayout.setVisibility(View.GONE);
                successLayout.setVisibility(View.VISIBLE);
                ok.setOnClickListener(v -> {
                    startActivity(new Intent(BuyTV.this,HomeActivity.class));
                });
            }
            else{
                animationLayout.setVisibility(View.GONE);
                cutosmerName.setVisibility(View.VISIBLE);
                cutosmerName.setText("Name: " +event.getMessage());
                pay.setText("Confirm Payment");
                pay.setOnClickListener( V -> {
                    animationLayout.setVisibility(View.VISIBLE);
                    tv.payForTV();
                });
            }

        }else{
            Toast.makeText(BuyTV.this,event.getMessage(),Toast.LENGTH_SHORT).show();
            animationLayout.setVisibility(View.GONE);
        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }


}
